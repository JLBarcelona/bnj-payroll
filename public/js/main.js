const main_path = '';

const loader = url_path('/img/loading.gif');

const load_img = '<center><img src="'+loader+'" alt="loading" width="18"></center>';

function user_guide(){
  $("#user_guide_form").modal('show');
}

function popover_show(){
  $('[data-toggle="popover"]').popover();
  $('[data-toggle="tooltip"]').tooltip();
}

$(document).ready(function(){
  popover_show();
});

function url_path(path){
	var url = window.location.href;
	var arr = url.split("/");
	var result = arr[0]+"//"+arr[2];
	return result + main_path + path;
}

function num_only(evt){
	var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode != 45 && charCode != 43 && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

  function show_img_qr(_this){
      $("#img_qr_prev").attr('src', _this.attr('src'));
      $("#qr_modal").modal('show');
  }



 function showValidator(validators,form) {
    var $inputs = $('#'+form+' .form-control');
     // An array of just the ids...
     var ids = {};
     $inputs.each(function (index)
     {
         // For debugging purposes...
         var ids = $(this).attr('id');
         $("#"+ids).removeClass('is-invalid');
         // console.log(ids);
         // ids[$(this).attr('name')] = $(this).attr('id');
     });

    $.each( validators, function( key, value ) {
        $("#err_"+key).text(value[0]);
        $("#"+key).addClass('is-invalid');
        // console.log(value[0].trim());
    });
    // $("#"+form).addClass('was-validated');

}

function load_waiting(title, message){
    swal({
       title: title,
       text: message,
       type: "",
       showCancelButton: false,
       showConfirmButton: false
     });
}

function loading(){
    swal({
       title: "loading",
       text: "Please wait...",
       type: "",
       timer: 1000,
       showCancelButton: false,
       showConfirmButton: false
     });
}

function remove_all_validation(){
   $("form.needs-validation :input").removeClass('is-invalid');
}

$(document).ready(function(){
  $("form.needs-validation :input").on('input', function(){
      var _this = $(this);
      var checkClass = _this.hasClass('is-invalid');
      if (checkClass) {
        _this.removeClass('is-invalid');
      }
      // alert();
  });
});

 $(document).ready(function(){
    $('.is_url').on('click', function(){
      var _this = $(this);

      if (_this.val()  == '' || _this.val()  == null) {
        _this.val('https://');
      }else{
        _this.val(_this.val());
      }

    });

     $('.is_url').on('focus', function(){
      var _this = $(this);

      if (_this.val()  == '' || _this.val()  == null) {
        _this.val('https://');
      }else{
        _this.val(_this.val());
      }

    });

     $('.is_url').on('blur', function(){
      var _this = $(this);

      if (_this.val()  == 'https://') {
        _this.val('');
      }else{
        _this.val(_this.val());
      }

    });

      // Add minus icon for collapse element which is open by default
      $(".collapse.show").each(function(){
        $(this).prev(".card-header").find(".fa").addClass("fa-minus").removeClass("fa-plus");
      });

      // Toggle plus minus icon on show hide of collapse element
      $(".collapse").on('show.bs.collapse', function(){
        $(this).prev(".card-header").find(".fa").removeClass("fa-plus").addClass("fa-minus");
      }).on('hide.bs.collapse', function(){
        $(this).prev(".card-header").find(".fa").removeClass("fa-minus").addClass("fa-plus");
      });
  });


  $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
// Sub menu

$('.dropdown-menu a.dropdown-toggles').on('click', function(e) {
  if (!$(this).next().hasClass('show')) {
    $(this).parents('.dropdown-menu').first().find('.show').removeClass('show');
  }
  var $subMenu = $(this).next('.dropdown-menu');
  $subMenu.toggleClass('show');


  $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
    $('.dropdown-submenu .show').removeClass('show');
  });


  return false;
});
