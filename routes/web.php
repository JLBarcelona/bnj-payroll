<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::namespace('Account')->group(function () {
	Route::get('/', function () {
	    return view('Account.index');
	});

	Route::post('/check_user', 'AccountController@login_user');
	Route::get('/logout/admin','AccountController@logout_admin')->name('logout.admin');

	Route::get('/logout/user','AccountController@logout_user')->name('logout.user');

 	Route::get('/forgot_password', function(){
	    return view('Account.forgot_password');
	});

	Route::post('get_token/{user_type}', 'ForgotPasswordController@get_token');
	Route::get('/new_password/{token}/{id}', 'ForgotPasswordController@new_password')->name('account.new_password');
	Route::get('/verify_account/{token}/{id}', 'AccountController@verify_account')->name('account.verify');

	Route::post('/create_new_password/{token}/{id}', 'ForgotPasswordController@create_new_password');

});

Route::namespace('Chat')->group(function () {
	Route::middleware(['auth'])->group(function () {
		// Chat
		Route::get('m/chat/{chat_id?}', 'ChatController@chat')->name('user.chat');

		Route::get('get/list/chat/{filter?}', 'ChatController@chat_list')->name('chat.list');

		Route::post('chat/send', 'ChatController@send_message')->name('send.chat');
		Route::post('get/chat', 'ChatController@get_chat')->name('get.chat');
	});
});

Route::namespace('Admin')->group(function () {
	Route::middleware(['auth'])->group(function () {
		Route::get('/system/settings', 'AdminController@settings')->name('admin.settings');

		Route::get('/admin', 'AdminController@index')->name('admin.index');
		// Payroll
		Route::post('upload/payroll', 'PayrollController@upload_excel')->name('payroll.add');
		Route::get('/attendance/list_attendance/{date?}', 'PayrollController@list_attendance')->name('attendance.list');
		Route::get('/attendance/delete_attendance/{daily_attendance_id}', 'PayrollController@delete_attendance')->name('attendance.delete');
		Route::post('/attendance/add_attendance', 'PayrollController@add_attendance')->name('attendance.add');

		Route::get('print/payslip/{pay_id}', 'ReportController@print_payslip')->name('payslip.print');

		// Salary
		Route::get('/salary/list_salary', 'PayrollController@list_salary')->name('salary.list');
		Route::post('/salary/add_salary', 'PayrollController@add_salary')->name('salary.add');
		Route::get('/salary/delete_salary/{salary_id}', 'PayrollController@delete_salary')->name('salary.delete');

		// Employees
		Route::get('/employees', 'EmployeeController@index')->name('employees.index');
		Route::get('/employees/payroll', 'EmployeeController@payroll')->name('employees.payroll');

		//AccountSetting
		Route::get('/admin/accountsetting', 'AdminController@accountsetting')->name('admin.accountsetting');
		Route::post('/admin/update_accountsetting', 'AdminController@update_accountsetting')->name('admin.update_accountsetting');

		// Change Password
		Route::post('/admin/changepassword', 'AdminController@changepassword')->name('user.changepassword');

		// Crud
		 Route::get('/employee/list_employee', 'EmployeeController@list_employee')->name('employee.list');
		 Route::post('/employee/add_employee', 'EmployeeController@add_employee')->name('employee.add');
		 Route::get('/employee/delete_employee/{user_id}', 'EmployeeController@delete_employee')->name('employee.delete');

		//BENEFITS
		Route::get('/benefits/list_benefits', 'BenefitController@list_benefits')->name('benefits.list');
		Route::post('/benefits/add_benefits', 'BenefitController@add_benefits')->name('benefits.add');
		Route::get('/benefits/delete_benefits/{benefit_id}', 'BenefitController@delete_benefits')->name('benefits.delete');

		//System Setting
		Route::post('/systemsetting/add_systemsetting', 'SystemSettingController@add_systemsetting')->name('systemsetting.add');

		// Late Config
		Route::get('/lateconfig/list_lateconfig', 'SystemSettingController@list_lateconfig')->name('lateconfig.list');
		Route::post('/lateconfig/add_lateconfig', 'SystemSettingController@add_lateconfig')->name('lateconfig.add');
		Route::get('/lateconfig/delete_lateconfig/{late_config_id}', 'SystemSettingController@delete_lateconfig')->name('lateconfig.delete');

		// Holiday
		Route::get('/holiday', 'HolidayController@index')->name('holiday.index');
		Route::get('/holiday/list_holiday', 'HolidayController@list_holiday')->name('holiday.list');
		Route::post('/holiday/add_holiday', 'HolidayController@add_holiday')->name('holiday.add');
		Route::get('/holiday/delete_holiday/{holiday_id}', 'HolidayController@delete_holiday')->name('holiday.delete');

	});
});


Route::namespace('User')->group(function () {
	Route::middleware(['auth'])->group(function () {
		Route::get('/user', 'UserController@index')->name('user.index');
		Route::get('/user/accountsettings', 'UserController@update_account')->name('user.index');

		//Attendance
		Route::get('/report/attendance', function () {
		    return view('User.attendance');
		});

		//Salary
		Route::get('/report/salary', function () {
		    return view('User.salary');
		});

		// AccountSetting
		Route::get('/user/list/choice', 'UserController@list_user_choice')->name('user.list.choice');

		Route::get('/user/list_user', 'UserController@list_user')->name('user.list');
		Route::post('/user/add_user', 'UserController@add_user')->name('user.add');
		Route::get('/user/delete_user/{user_id}', 'UserController@delete_user')->name('user.delete');

		// Change Password
		Route::post('/user/changepassword', 'UserController@change_password')->name('user.changepassword');


		// Report
		Route::get('/user/attendance/list_attendance/{date?}', 'ReportController@list_attendance_user')->name('attendance.list.user');
		Route::get('/user/salary/list_salary/{date?}', 'ReportController@list_salary_user')->name('salary.list.user');
		Route::get('user/print/payslip/{pay_id}', 'ReportController@print_payslip')->name('payslip.print');


	});
});
