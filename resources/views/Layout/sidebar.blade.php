@php
  $parent = '';
  $child = '';
  $active = url()->current();
@endphp

@if($type == 'home')

@elseif($type == 'user')
 <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="../../index3.html" class="brand-link">
      <img src="{{ asset('img/logo.png') }}"
           alt="AdminLTE Logo"
           class="brand-image img-circle elevation-3 bg-white">
           <span class="brand-text font-weight-light">User</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar" >
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          @if(!empty(Auth::user()->profile_picture))
            <img src="{{ asset('profile_picture/'.Auth::user()->profile_picture) }}" class="img-circle elevation-2" alt="User Image">
          @else
            <img src="{{ asset('img/avatar.png') }}" class="img-circle elevation-2" alt="User Image">
          @endif
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ Auth::user()->fullname }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview {{ (request()->is('report/salary') || request()->is('report/attendance')) ? 'menu-open' : '' }} ">
            <a href="#" class="nav-link {{ (request()->is('report/salary') || request()->is('report/attendance')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Report
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('report/attendance') }}" class="nav-link {{ (request()->is('report/attendance')) ? 'active' : '' }}">
                  <i class="nav-icon fas fa-calendar"></i>
                  <p>Attendance</p>
                </a>
              </li>
              <li class="nav-item active">
                <a href="{{ url('report/salary') }}" class="nav-link {{ (request()->is('report/salary')) ? 'active' : '' }}">
                  <i class="nav-icon fas fa-money-check-alt"></i>
                  <p>Salary</p>
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
@elseif($type == 'admin')
 <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="../../index3.html" class="brand-link">
      <img src="{{ asset('img/logo.png') }}"
           alt="AdminLTE Logo"
           class="brand-image img-circle elevation-3 bg-white">
           <span class="brand-text font-weight-light">Admin</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar" >
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          @if(!empty(Auth::user()->profile_picture))
            <img src="{{ asset('profile_picture/'.Auth::user()->profile_picture) }}" class="img-circle elevation-2" alt="User Image">
          @else
            <img src="{{ asset('img/avatar.png') }}" class="img-circle elevation-2" alt="User Image">
          @endif
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ Auth::user()->fullname }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="{{ url('admin') }}" class="nav-link {{ (request()->is('admin')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview {{ (request()->is('employees') || request()->is('employees/payroll')) ? 'menu-open' : '' }} ">
            <a href="#" class="nav-link {{ (request()->is('employees') || request()->is('employees/payroll')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Employees
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('employees') }}" class="nav-link {{ (request()->is('employees')) ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Employees Account</p>
                </a>
              </li>
              <li class="nav-item active">
                <a href="{{ url('employees/payroll') }}" class="nav-link {{ (request()->is('employees/payroll')) ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Payroll</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="{{ url('/system/settings') }}" class="nav-link {{ (request()->is('system/settings')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-th"></i>
              <p>
                System Settings
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('/holiday') }}" class="nav-link {{ (request()->is('holiday')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-calendar"></i>
              <p>
                Holiday
              </p>
            </a>
          </li>
          <!-- <li class="nav-header">EXAMPLES</li> -->
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
@endif
