<!DOCTYPE html>
<html>
  <!-- Header css meta -->
@include('Layout.header', ['type' => 'admin', 'title' => 'Chat', 'icon' => asset('img/logo.png') ])
  <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
  <body class="sidebar-mini layout-fixed" onload="get_list_chat(); ">
  <div class="wrapper">
  <!-- navbar -->
  @include('Layout.nav', ['type' => 'admin'])
  <!-- Sidebar -->
  @include('Layout.sidebar', ['type' => 'admin'])
   <div class="content-wrapper">
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-4 mt-3 web-only">
              <div class="card direct-chat direct-chat-primary direct-chat-contacts-open ">
                <!-- /.card-header -->
                <div class="card-header"><i class="fa fa-users"></i> Users</div>
                <div class="card-body">
                  <!-- Conversations are loaded here -->
                  <div class="direct-chat-messages" style="height: 510px; ">
                  </div>
                  <!--/.direct-chat-messages-->

                  <!-- Contacts are loaded here -->
                  <div class="direct-chat-contacts bg-white text-dark" style="height: 510px; overflow-x: hidden;">
                    <div class="row p-2">
                      <div class="col-sm"><a href="{{ url('/m/chat/') }}" class="btn btn-block btn-success"><i class="fa fa-plus-circle"></i></a></div>
                       <div class="col-sm-10">
                        <input type="text" id="search" name="search" class="form-control" placeholder="Search">
                      </div>
                      <div class="col-sm-12">
                        <hr>
                      </div>
                    </div>
                    
                    <ul class="contacts-list">
                      <!-- Contact here -->
                    </ul>
                  </div>
                  <!-- /.direct-chat-pane -->
                </div>
              </div>
            </div>
            <div class="col-sm-8 mt-3">

              @if(!empty($chat->chat_id))

              @else
                <form action="{{ url('chat/send') }}" method="post">
              @endif

              <input type="text" name="chat_id_box" id="chat_id_box" value="{{ $chat->chat_id ?? '' }}">
              <div class="card direct-chat direct-chat-primary" id="chat_box" data-chat-id="{{ $chat->chat_id ?? '' }}" >
                <div class="card-header ui-sortable-handle" style="">
                  
                    @if(!empty($chat->chat_id))
                    <h3 class="card-title bold">
                      {{ $chat->group ?? ''}}
                    </h3>
                    @else
                    <div class="row">
                      <div class="col-sm-12">
                         <div class="form-group">
                            <label>Compose Message</label>
                            <div class="select2-purple">
                              <select class="select2 form-control hide" name="user_receipients" id="user_receipients" multiple="multiple" data-placeholder="Select Receipient" data-dropdown-css-class="select2-purple" >
                                @foreach($users as $user)
                                <option value="{{ $user->user_id }}">{{ $user->fullname }}</option>

                                @endforeach
                              </select>
                            </div>
                          </div>
                      </div>
                    </div>
                    @endif
                
                  <div class="card-tools">
                    <button type="button" class="btn btn-tool mobile-only" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle">
                      <i class="fas fa-comments"></i>
                    </button>
                   <!--  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
                    </button> -->
                  </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <!-- Conversations are loaded here -->
                  <div class="direct-chat-messages" style="height: 360px;">
                  <!-- Message Here -->
                  </div>
                  <!--/.direct-chat-messages-->

                  <!-- Contacts are loaded here -->
                  <div class="direct-chat-contacts bg-white text-dark" style="height: 360px;">
                    <ul class="contacts-list ">
                     <!-- Contact here -->
                    </ul>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <div class="input-group">
                      <input type="text" name="message" id="message" placeholder="Type Message ..." class="form-control">
                      <span class="input-group-append">
                        <button type="submit" class="btn btn-primary">Send</button>
                      </span>
                    </div>
                </div>
                <!-- /.card-footer-->
              </div>

               @if(!empty($chat->chat_id))

              @else
                </form>
              @endif

            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</body>
  <!-- Footer Scripts -->
  @include('Layout.footer', ['type' => 'admin'])
  <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
  <script src="{{ asset('chat/index.js') }}"></script>
  <script src="{{ asset('plugins/select2/js/select2.full.min.js') }}"></script>
</html>

<script type="text/javascript">
  // get_chat(35);

  function get_list_chat(){
    let url = main_path + '/get/list/chat';
    $.ajax({
        type:"GET",
        url:url,
        data:{},
        dataType:'json',
        beforeSend:function(){
          $(".contacts-list").html(load_img);
        },
        success:function(response){
          console.log(response);
         if (response.status == true) {
          $(".contacts-list").html(response.data);
          $('.select2').select2();
          $(".select2").removeClass('hide');
         }else{
          console.log(response);
         }
        },
        error: function(error){
          console.log(error);
        }
      });
  }
</script>


