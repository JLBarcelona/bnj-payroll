<!DOCTYPE html>
<html>
  <!-- Header css meta -->
  @include('Layout.header', ['type' => 'home', 'title' => 'login', 'icon' => asset('img/logo.png') ])
<body class="hold-transition login-page">
<div class="login-box ">
  <div class="login-logo">
    <img src="{{ asset('img/logo.png') }}" alt="">
    <p><b>Brain Network Japan</b></p>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Login</p>
      <form class="needs-validation" id="login_form" action="{{ url('/check_user') }}" novalidate>
        <div class="input-group mb-3">
          <input type="email" name="email_address" id="email_address" class="form-control" placeholder="Email">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
          <div class="invalid-feedback" id="err_email_address"></div>

        </div>
        <div class="input-group mb-3">
          <input type="password" name="password" id="password" class="form-control" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
          <div class="invalid-feedback" id="err_password"></div>
        </div>
        <div class="row mb-3">
          <div class="col-8">
             <a href="forgot-password.html">I forgot my password</a>
          </div>
          <!-- /.col -->
          <div class="col text-right">
            <button type="submit" class="btn btn-primary btn-block btn-sm">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
</body>
  <!-- Footer Scripts -->
  @include('Layout.footer', ['type' => 'home'])
</html>

<script type="text/javascript">
  $("#login_form").on('submit', function(e){
    var url = $(this).attr('action');
    var mydata = $(this).serialize();
    e.stopPropagation();
    e.preventDefault(e);

    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){
          //<!-- your before success function -->
      },
      success:function(response){
          console.log(response);
        if(response.status == true){
          // console.log(response);
          if (response.user_type == 1) {
            window.location = main_path + '/admin';
          }else if (response.user_type == 2) {
            window.location = main_path + '/user';
          }
          // swal("Success", response.message, "success");
          showValidator(response.error,'login_form');
        }else{
          //<!-- your error message or action here! -->
          showValidator(response.error,'login_form');
        }
      },
      error:function(error){
        console.log(error)
      }
    });
  });
</script>