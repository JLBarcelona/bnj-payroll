<!DOCTYPE html>

<html>
  <!-- Header css meta -->
   @include('Layout.header', ['type' => 'admin', 'title' => 'Admin', 'icon' => asset('img/logo.png') ])

<body class="sidebar-mini layout-fixed" onload="show_employee();">
  <div class="wrapper">
  <!-- navbar -->
  @include('Layout.nav', ['type' => 'admin'])
  <!-- Sidebar -->
  @include('Layout.sidebar', ['type' => 'admin'])
   <div class="content-wrapper">
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12 mt-3">
              <div class="card">
                <div class="card-header h4">
                  <i class="fa fa-users"></i> Employees Account
                  <button class="btn btn-primary float-right btn-sm" onclick="add_employee()"><i class="fa fa-plus"></i> Add Employee</button>
                </div>
                <div class="card-body">
                  <table class="table table-bordered dt-responsive nowrap" id="tbl_employee" style="width: 100%;"></table>
                </div>
                <div class="card-footer"></div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</body>

    <div class="modal fade" role="dialog" id="modal_add_employee">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <div class="modal-title">
              Employee
            </div>
            <button class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <!--NAVIgation bar-->
            <form  class="needs-validation" id="add_form_employee" action="{{ url('employee/add_employee') }}" novalidate>
              <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" id="page1" data-toggle="tab" href="#page1_tab" role="tab" aria-controls="page1_tab" aria-selected="true">Profile</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="page2" data-toggle="tab" href="#page2_tab" role="tab" aria-controls="page2_tab" aria-selected="false">Benefits</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="page3" data-toggle="tab" href="#page3_tab" role="tab" aria-controls="page3_tab" aria-selected="false">Account</a>
                </li>
              </ul>
              <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active pt-2" id="page1_tab" role="tabpanel" aria-labelledby="page1">
                  <input type="hidden" id="user_id" name="user_id" placeholder="" class="form-control" required>
                   <div class="form-group col-sm-12">
                    <label>Attendance ID </label>
                    <input type="text" id="attendance_id" name="attendance_id" placeholder="Attendance ID" class="form-control " required>
                    <div class="invalid-feedback" id="err_attendance_id"></div>
                  </div>
                  <div class="form-group col-sm-12">
                    <label>Firstname </label>
                    <input type="text" id="firstname" name="firstname" placeholder="Firstname" class="form-control " required>
                    <div class="invalid-feedback" id="err_firstname"></div>
                  </div>
                  <div class="form-group col-sm-12">
                    <label>Lastname </label>
                    <input type="text" id="lastname" name="lastname" placeholder="Lastname" class="form-control " required>
                    <div class="invalid-feedback" id="err_lastname"></div>
                  </div>
                  <div class="form-group col-sm-12">
                    <label>Middlename </label>
                    <input type="text" id="middlename" name="middlename" placeholder="Middlename" class="form-control " required>
                    <div class="invalid-feedback" id="err_middlename"></div>
                  </div>
                  <div class="form-group col-sm-12">
                    <label>Basic Salary </label>
                    <input type="text" id="basic_salary" name="basic_salary" placeholder="Basic Salary" class="form-control " required>
                    <div class="invalid-feedback" id="err_basic_salary"></div>
                  </div>
                  <div class="col-sm-12 text-right">

                    <button class="btn btn-success btn-sm" onclick="next_tab('page2', 'page1', 'page3');" type="button">Next</button>
                  </div>
                </div>
                <div class="tab-pane fade pt-2" id="page2_tab" role="tabpanel" aria-labelledby="page2">
                  <div class="form-row">
                    <input type="hidden" id="benefit_id" name="benefit_id" placeholder="" class="form-control" required>
                    <div class="form-group col-sm-6">
                      <label>SSS Number </label>
                      <input type="text" id="sss_number" name="sss_number" placeholder="SSS Number" class="form-control form-control" required>
                      <div class="invalid-feedback" id="err_sss_number"></div>
                    </div>
                    <div class="form-group col-sm-6">
                      <label>SSS Share </label>
                      <input type="text" id="sss_share" name="sss_share" placeholder="SSS Share" class="form-control form-control" required>
                      <div class="invalid-feedback" id="err_sss_share"></div>
                    </div>
                    <div class="form-group col-sm-6">
                      <label>PhilHealth Number </label>
                      <input type="text" id="phil_health_number" name="phil_health_number" placeholder="PhilHealth Number" class="form-control form-control" required>
                      <div class="invalid-feedback" id="err_phil_health_number"></div>
                    </div>
                    <div class="form-group col-sm-6">
                      <label>PhilHealth Share </label>
                      <input type="text" id="phil_health_share" name="phil_health_share" placeholder="PhilHealth Share" class="form-control form-control" required>
                      <div class="invalid-feedback" id="err_phil_health_share"></div>
                    </div>
                    <div class="form-group col-sm-6">
                      <label>Pag-ibig Number </label>
                      <input type="text" id="pag_ibig_number" name="pag_ibig_number" placeholder="Pag-ibig Number" class="form-control form-control" required>
                      <div class="invalid-feedback" id="err_pag_ibig_number"></div>
                    </div>
                    <div class="form-group col-sm-6">
                      <label>Pag-ibig Share </label>
                      <input type="text" id="pag_ibig_share" name="pag_ibig_share" placeholder="Pag-ibig Share" class="form-control form-control" required>
                      <div class="invalid-feedback" id="err_pag_ibig_share"></div>
                    </div>
                    <div class="col-sm-12 text-right">
                      <!--<button class="btn btn-success" type="submit">Submit</button>-->
                      <button class="btn btn-dark btn-sm" type="button" onclick="next_tab('page1', 'page2', 'page3');">Previous</button>
                       <button class="btn btn-success btn-sm" type="button" onclick="next_tab('page3', 'page1', 'page2');">Next</button>
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade pt-2" id="page3_tab" role="tabpanel" aria-labelledby="page3">
                  <div class="form-group col-sm-12">
                    <label>Username </label>
                    <input type="text" id="username" name="username" placeholder="Username" class="form-control " required>
                    <div class="invalid-feedback" id="err_username"></div>
                  </div>
                  <div class="form-group col-sm-12">
                    <label>Email Address </label>
                    <input type="text" id="email_address" name="email_address" placeholder="Email Address" class="form-control " required>
                    <div class="invalid-feedback" id="err_email_address"></div>
                  </div>
                  <div class="col-sm-12 text-right">
                    <button class="btn btn-dark btn-sm" type="button" onclick="next_tab('page2', 'page1', 'page3');">Previous</button>
                    <button class="btn btn-success btn-sm" type="submit">Save</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>

  <!-- Footer Scripts -->
  @include('Layout.footer', ['type' => 'admin'])
</html>

<script type="text/javascript">
  function next_tab(show, hide1, hide2){
    $("#"+show).addClass('active');
    $("#"+hide1).removeClass('active');
    $("#"+hide2).removeClass('active');
    
    $("#"+show+'_tab').addClass('show active');
    $("#"+hide1+'_tab').removeClass('show active');
    $("#"+hide2+'_tab').removeClass('show active');
  }

  function add_employee(){
    $("#modal_add_employee").modal('show');
  }

  function add_benefits(){

    $("#modal_add_benefit").modal('show');

    $("#modal_add_employee").modal('hide');
  }
</script>


<script>
  var tbl_employee;
  function show_employee(){
    if (tbl_employee) {
      tbl_employee.destroy();
    }
    var url = main_path + '/employee/list_employee';
    tbl_employee = $('#tbl_employee').DataTable({
    pageLength: 10,
    responsive: true,
    ajax: url,
    deferRender: true,
    language: {
    "emptyTable": "No data available"
  },
    columns: [{
    className: '',
    "data": "attendance_id",
    "title": "Attendance ID",
  },{
    className: '',
    "data": "lastname",
    "title": "Lastname",
  },{
    className: '',
    "data": "firstname",
    "title": "Firstname",
  },{
    className: '',
    "data": "username",
    "title": "Username",
  },{
    className: '',
    "data": "email_address",
    "title": "Email_address",
  },{
    className: 'width-option-1 text-center',
    "data": "user_id",
    "orderable": false,
    "title": "Options",
      "render": function(data, type, row, meta){
        var param_data = JSON.stringify(row);
        newdata = '';
        newdata += '<button class="btn btn-success btn-sm font-base mt-1" data-info=\' '+param_data.trim()+'\' onclick="edit_employee(this)" type="button"><i class="fa fa-edit"></i> Edit</button>';
        newdata += ' <button class="btn btn-danger btn-sm font-base mt-1" data-info=\' '+param_data.trim()+'\' onclick="delete_employee(this)" type="button"><i class="fa fa-edit"></i> Delete</button>';
        return newdata;
      }
    }
  ]
  });
  }

  $("#add_form_employee").on('submit', function(e){
    var url = $(this).attr('action');
    var mydata = $(this).serialize();
    e.stopPropagation();
    e.preventDefault(e);

    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){
          //<!-- your before success function -->
      },
      success:function(response){
          //console.log(response)
        if(response.status == true){
          // console.log(response)
          show_employee();
          swal("Success", response.message, "success");
          showValidator(response.error,'add_form_employee');
          $('#modal_add_employee').modal('hide');
					$('body').removeClass('modal-open');
					$('.modal-backdrop').remove();
        }else{
          //<!-- your error message or action here! -->
          showValidator(response.error,'add_form_employee');
        }
      },
      error:function(error){
        console.log(error)
      }
    });
  });

  function delete_employee(_this){
    var data = JSON.parse($(_this).attr('data-info'));
    var url =  main_path + '/employee/delete_employee/' + data.user_id;
      swal({
        title: "Are you sure?",
        text: "Do you want to delete this employee?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        closeOnConfirm: false
      },
      function(){
        $.ajax({
        type:"GET",
        url:url,
        data:{},
        dataType:'json',
        beforeSend:function(){
      },
      success:function(response){
        // console.log(response);
        if (response.status == true) {
          show_employee();
          swal("Success", response.message, "success");
        }else{
          console.log(response);
        }
      },
      error: function(error){
        console.log(error);
      }
      });
    });
  }

  function edit_employee(_this){
    add_employee();
    var data = JSON.parse($(_this).attr('data-info'));
    $('#user_id').val(data.user_id);
    $('#firstname').val(data.firstname);
    $('#lastname').val(data.lastname);
    $('#middlename').val(data.middlename);
    $('#username').val(data.username);
    $('#email_address').val(data.email_address);
    $('#attendance_id').val(data.attendance_id);
    $('#basic_salary').val(data.basic_salary);

  }

//<!--BENEFITS-->


    var tbl_benefits;
    function show_benefits(){
      if (tbl_benefits) {
        tbl_benefits.destroy();
      }
      var url = main_path + '/benefits/list_benefits';
      tbl_benefits = $('#tbl_benefits').DataTable({
      pageLength: 10,
      responsive: true,
      ajax: url,
      deferRender: true,
      language: {
      "emptyTable": "No data available"
    },
      columns: [{
      className: '',
      "data": "sss_number",
      "title": "Sss_number",
    },{
      className: '',
      "data": "sss_share",
      "title": "Sss_share",
    },{
      className: '',
      "data": "phil_health_number",
      "title": "Phil_health_number",
    },{
      className: '',
      "data": "phil_health_share",
      "title": "Phil_health_share",
    },{
      className: '',
      "data": "pag_ibig_number",
      "title": "Pag_ibig_number",
    },{
      className: '',
      "data": "pag_ibig_share",
      "title": "Pag_ibig_share",
    },{
      className: 'width-option-1 text-center',
      "data": "benefit_id",
      "orderable": false,
      "title": "Options",
        "render": function(data, type, row, meta){
          var param_data = JSON.stringify(row);
          newdata = '';
          newdata += '<button class="btn btn-success btn-sm font-base mt-1" data-info=\' '+param_data.trim()+'\' onclick="edit_benefits(this)" type="button"><i class="fa fa-edit"></i> Edit</button>';
          newdata += '<button class="btn btn-danger btn-sm font-base mt-1" data-info=\' '+param_data.trim()+'\' onclick="delete_benefits(this)" type="button"><i class="fa fa-edit"></i> delete</button>';
          return newdata;
        }
      }
    ]
    });
    }

    $("#benefit_form_id").on('submit', function(e){
      var url = $(this).attr('action');
      var mydata = $(this).serialize();
      e.stopPropagation();
      e.preventDefault(e);

      $.ajax({
        type:"POST",
        url:url,
        data:mydata,
        cache:false,
        beforeSend:function(){
            //<!-- your before success function -->
        },
        success:function(response){
            //console.log(response)
          if(response.status == true){
            console.log(response)
            swal("Success", response.message, "success");
            showValidator(response.error,'benefit_form_id');
          }else{
            //<!-- your error message or action here! -->
            showValidator(response.error,'benefit_form_id');
          }
        },
        error:function(error){
          console.log(error)
        }
      });
    });

    function delete_benefits(_this){
      var data = JSON.parse($(_this).attr('data-info'));
      var url =  main_path + '/benefits/delete_benefits/' + data.benefit_id;
        swal({
          title: "Are you sure?",
          text: "Do you want to delete this benefits?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes",
          closeOnConfirm: false
        },
        function(){
          $.ajax({
          type:"GET",
          url:url,
          data:{},
          dataType:'json',
          beforeSend:function(){
        },
        success:function(response){
          // console.log(response);
          if (response.status == true) {
            swal("Success", response.message, "success");
          }else{
            console.log(response);
          }
        },
        error: function(error){
          console.log(error);
        }
        });
      });
    }

    function edit_benefits(_this){
      var data = JSON.parse($(_this).attr('data-info'));
      $('#benefit_id').val(data.benefit_id);
      $('#sss_number').val(data.sss_number);
      $('#sss_share').val(data.sss_share);
      $('#phil_health_number').val(data.phil_health_number);
      $('#phil_health_share').val(data.phil_health_share);
      $('#pag_ibig_number').val(data.pag_ibig_number);
      $('#pag_ibig_share').val(data.pag_ibig_share);
    }

</script>
