<!DOCTYPE html>
<html>
  <!-- Header css meta -->
   @include('Layout.header', ['type' => 'admin', 'title' => 'Employees', 'icon' => asset('img/logo.png') ])
<body class="sidebar-mini layout-fixed" onload="show_lateconfig();">
  <div class="wrapper">
  <!-- navbar -->
  @include('Layout.nav', ['type' => 'admin'])
  <!-- Sidebar -->
  @include('Layout.sidebar', ['type' => 'admin'])
    <div class="content-wrapper">
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-12 mt-3">
              <div class="card">
				<div class="card-header h4"><i class="fa fa-money-check-alt"></i> <span>Base Salary Settings</span></div>
                <div class="card-body">
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="card">
                        <div class="card-header">Salary Settings</div>
                        <!-- <div class="card-body"> -->
                          <!-- Content here -->
                          <div>
                          <ul class="list-group list-group-flush">
                            <li class="list-group-item">Work Hour/Day <span class="float-right">{{ Auth::user()->salaryconfig->total_hour_per_day }}</span></li>
                            <li class="list-group-item">Holiday Rate <span class="float-right">{{ Auth::user()->salaryconfig->holiday_rate }}</span></li>
                          </ul>
                          </div>
                        <!-- </div> -->
                        <div class="card-footer"></div>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="card">
                        <div class="card-header">Salary Configuration</div>
                        <div class="card-body">
                          <!-- Content here -->
                          <form class="needs-validation" id="salaryconfig_id" action="{{ url('/systemsetting/add_systemsetting') }}" novalidate>
                            <div class="form-row">
                              <input type="hidden" id="config_id" name="config_id" placeholder="" value="{{ Auth::user()->salaryconfig->config_id }}" class="form-control" required>
                              <div class="form-group col-sm-12">
                                <label>Total Hour Per Day </label>
                                <input type="number" id="total_hour_per_day" name="total_hour_per_day" placeholder="Total Hour Per Day" value="{{ Auth::user()->salaryconfig->total_hour_per_day }}" class="form-control " required>
                                <div class="invalid-feedback" id="err_total_hour_per_day"></div>
                              </div>
                              <div class="form-group col-sm-12">
                                <label>Holiday Rate </label>
                                <input type="number" id="holiday_rate" name="holiday_rate" placeholder="Holiday Rate" value="{{ Auth::user()->salaryconfig->holiday_rate }}" class="form-control " required>
                                <div class="invalid-feedback" id="err_holiday_rate"></div>
                              </div>

                              <div class="col-sm-12 text-right">
                                <button class="btn btn-secondary" type="submit">Save</button>
                              </div>
                            </div>
                          </form>
                        </div>
                        <div class="card-footer"></div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-12">
                      <hr>
                    </div>
                  </div>


                </div>
                <div class="card-footer"></div>
              </div>

              <div class="card">
                <div class="card-header h4"><i class="fas fa-clock"></i> <span>Late Deduction Settings</span>
					<button class="btn btn-primary btn-sm float-right" onclick="form_reset(); add_late()"><i class="fa fa-plus"></i> Add Late</button>
				</div>
				<div class="card-body">
                  <table class="table table-bordered dt-responsive nowrap" id="tbl_lateconfig" style="width: 100%;"></table>
                </div>
                <div class="card-footer"></div>
              </div>

            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</body>

 <div class="modal fade" role="dialog" id="modal_add_late_settings">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <div class="modal-title">
            Add Late Settings
            </div>
            <button class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
             <form class="needs-validation" id="late_config" action="{{ url('/lateconfig/add_lateconfig') }}" novalidate>
              <div class="form-row">
                  <input type="hidden" id="late_config_id" name="late_config_id" placeholder="" class="form-control" required>
                  <div class="form-group col-sm-12">
                    <label>From Time </label>
                    <input type="text" id="from_time" name="from_time" placeholder="Start Time" class="form-control " required>
                    <div class="invalid-feedback" id="err_from_time"></div>
                  </div>
                  <div class="form-group col-sm-12">
                    <label>End Time </label>
                    <input type="text" id="end_time" name="end_time" placeholder="End Time" class="form-control "  required>
                    <div class="invalid-feedback" id="err_end_time"></div>
                  </div>
                  <div class="form-group col-sm-12">
                    <label>Deducted From Time </label>
                    <input type="text" id="deducted_from_time" name="deducted_from_time" placeholder="Deducted Time" class="form-control " required>
                    <div class="invalid-feedback" id="err_deducted_from_time"></div>
                  </div>

                  <div class="col-sm-12 text-right">
                    <button class="btn btn-secondary" type="submit">Save</button>
                  </div>
                </div>
              </form>
          </div>
          <div class="modal-footer">

          </div>
        </div>
      </div>
    </div>
  <!-- Footer Scripts -->
  @include('Layout.footer', ['type' => 'admin'])
</html>

<!-- Javascript Function-->
<script>

  function form_reset(){
    $("#late_config_id").val('');
    document.getElementById('late_config').reset();
  }

  function add_late(){
    $("#modal_add_late_settings").modal('show');
  }

  $("#salaryconfig_id").on('submit', function(e){
		var url = $(this).attr('action');
		var mydata = $(this).serialize();
		e.stopPropagation();
		e.preventDefault(e);

		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
			},
			success:function(response){
					//console.log(response)
				if(response.status == true){
					console.log(response)
					swal("Success", response.message, "success");
					showValidator(response.error,'salaryconfig_id');
          form_reset();
          $("#modal_add_late_settings").modal('hide');
				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'salaryconfig_id');
				}
			},
			error:function(error){
				console.log(error)
			}
		});
	});

</script>



<script>

	var tbl_lateconfig;
	function show_lateconfig(){
		if (tbl_lateconfig) {
			tbl_lateconfig.destroy();
		}
		var url = main_path + '/lateconfig/list_lateconfig';
		tbl_lateconfig = $('#tbl_lateconfig').DataTable({
		pageLength: 10,
		responsive: true,
		ajax: url,
		deferRender: true,
		language: {
		"emptyTable": "No data available"
	},
		columns: [{
		className: '',
		"data": "from_time",
		"title": "From Time",
	},{
		className: '',
		"data": "end_time",
		"title": "End Time",
	},{
		className: '',
		"data": "deducted_from_time",
		"title": "Deducted from Time",
	},{
		className: 'width-option-1 text-center',
		"data": "late_config_id",
		"orderable": false,
		"title": "Options",
			"render": function(data, type, row, meta){
				var param_data = JSON.stringify(row);
				newdata = '';
				newdata += '<button class="btn btn-success btn-sm font-base mt-1" data-info=\' '+param_data.trim()+'\' onclick="edit_lateconfig(this)" type="button"><i class="fa fa-edit"></i> Edit</button>';
				newdata += ' <button class="btn btn-danger btn-sm font-base mt-1" data-info=\' '+param_data.trim()+'\' onclick="delete_lateconfig(this)" type="button"><i class="fa fa-edit"></i> Delete</button>';
				return newdata;
			}
		}
	]
	});
	}

	$("#late_config").on('submit', function(e){
		var url = $(this).attr('action');
		var mydata = $(this).serialize();
		e.stopPropagation();
		e.preventDefault(e);

		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
			},
			success:function(response){
					//console.log(response)
				if(response.status == true){
					console.log(response)
					swal("Success", response.message, "success");
					showValidator(response.error,'late_config');
					$('#modal_add_late_settings').modal('hide');
					$('body').removeClass('modal-open');
					$('.modal-backdrop').remove();
          show_lateconfig();
				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'late_config');
				}
			},
			error:function(error){
				console.log(error)
			}
		});
	});

	function delete_lateconfig(_this){
		var data = JSON.parse($(_this).attr('data-info'));
		var url =  main_path + '/lateconfig/delete_lateconfig/' + data.late_config_id;
			swal({
				title: "Are you sure?",
				text: "Do you want to delete this Late Deduction Settings?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes",
				closeOnConfirm: false
			},
			function(){
				$.ajax({
				type:"GET",
				url:url,
				data:{},
				dataType:'json',
				beforeSend:function(){
			},
			success:function(response){
				// console.log(response);
				if (response.status == true) {
					swal("Success", response.message, "success");
          show_lateconfig();
				}else{
					console.log(response);
				}
			},
			error: function(error){
				console.log(error);
			}
			});
		});
	}

	function edit_lateconfig(_this){
		var data = JSON.parse($(_this).attr('data-info'));
		$('#late_config_id').val(data.late_config_id);
		$('#from_time').val(data.from_time);
		$('#end_time').val(data.end_time);
		$('#deducted_from_time').val(data.deducted_from_time);
    add_late();
	}
</script>
