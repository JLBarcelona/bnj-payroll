<!DOCTYPE html>
<html>
  <!-- Header css meta -->
   @include('Layout.header', ['type' => 'admin', 'title' => 'Payroll', 'icon' => asset('img/logo.png') ])
   <link rel="stylesheet" href="{{ asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">

<body class="sidebar-mini layout-fixed" onload="show_attendance(); ">
  <div class="wrapper">
  <!-- navbar -->
  @include('Layout.nav', ['type' => 'admin'])
  <!-- Sidebar -->
  @include('Layout.sidebar', ['type' => 'admin'])
    <div class="content-wrapper">
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-12 mt-3">
              <div class="card">
                <div class="card-header h4"><i class="fa fa-receipt"></i> <span>Payroll</span>
                  <button class="btn btn-sm btn-primary float-right" onclick="upload();"><i class="fa fa-upload"></i> Upload</button>
                </div>
                <div class="card-body">
                  <form class="needs-validation" id="salry_form" action="{{ url('salary/add_salary') }}" novalidate data-print="{{ url('print/payslip') }}">
                    <input type="hidden" name="pay_id" id="pay_id">
                    
                    <div class="card ">
                      <div class="card-body">
                        <div class="form-row">
                          <div class="col-sm">
                            <div class="input-group date" id="reservationdate" data-target-input="nearest">
                              <div class="input-group-prepend" data-target="#reservationdate" data-toggle="datetimepicker">
                                <div class="input-group-text">Date</div>
                              </div>
                              <input type="text" id="date_report" name="date_report" value="" class="form-control datetimepicker-input" data-target="#reservationdate">
                              <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                              </div>
                            <div class="invalid-feedback" id="err_date_report"></div>
                            </div>
                          </div>
                          <div class="col-sm-1">
                            <div class="form-group">
                              <button class="btn btn-block btn-success" type="button" onclick="show_attendance()"><i class="fa fa-search"></i></button>
                            </div>
                          </div>
                          <div class="col-sm-2">
                            <div class="form-group">
                              <button class="btn btn-block btn-dark"><i class="fa fa-calculator"></i> <small>Calculate Payroll</small></button>
                            </div>
                          </div>
                            <div class="col-sm-2">
                              <div class="form-group">
                                <a href="" id="btn_print" class="btn btn-block btn-info disable"><i class="fa fa-print"></i> <small>Print Payslip </small></a>
                              </div>
                            </div>
                          <!--       <div class="col-sm">
                            <div class="form-group">
                              <button class="btn btn-block btn-primary"></button>
                            </div>
                          </div> -->
                        </div>
                      </div>
                    </div>
                  </form>
                  <table class="table table-bordered dt-responsive nowrap" id="tbl_attendance" style="width: 100%;"></table>
                </div>
                <div class="card-footer"></div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</body>
  <!-- Footer Scripts -->
  @include('Layout.footer', ['type' => 'admin'])
  <script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
  <script src="{{ asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
</html>


<form id="payroll_form" action="{{ url('upload/payroll') }}" method="post" enctype="multipart/form-data">
 <div class="modal fade" role="dialog" id="file_upload">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <div class="modal-title">
            Upload Attendance
            </div>
            <button class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            
            <input type="hidden" id="payroll_id" name="payroll_id" placeholder="" class="form-control">
            <div class="form-row">
              <div class="form-group col-sm-12">
                <div class="custom-file">
                  <input type="file" class="custom-file-input" onchange="previewFile();" id="file" name="file">
                  <label class="custom-file-label" for="validatedCustomFile" id="filechoose">Choose file</label>
                  <div class="invalid-feedback" id="err_file"></div>
                </div>
              </div>
            </div>

          </div>
          <div class="modal-footer">
            <button class="btn btn-default btn-sm" type="button" data-dismiss="modal">Close</button>
            <button class="btn btn-dark btn-sm" type="submit">Upload</button>
          </div>
        </div>
      </div>
    </div>
  </form>


   <div class="modal fade" role="dialog" id="modal_edit_attendance">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <div class="modal-title">
              Date Attendance
              </div>
              <button class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
              <form class="needs-validation" id="sample_form_id" action="{{ url('attendance/add_attendance') }}" novalidate>
                <div class="form-row">
                  <input type="hidden" id="daily_attendance_id" name="daily_attendance_id">
                  <input type="hidden" id="attendance_id" name="attendance_id" placeholder="" class="form-control" required>
                  <div class="form-group col-sm-6">
                    <label>Time In(First)</label>
                    <input type="text" id="f_time_in" name="f_time_in" placeholder="" class="form-control " required>
                    <div class="invalid-feedback" id="err_f_time_in"></div>
                  </div>
                  <div class="form-group col-sm-6">
                    <label>Time Out(First)</label>
                    <input type="text" id="f_time_out" name="f_time_out" placeholder="" class="form-control " required>
                    <div class="invalid-feedback" id="err_f_time_out"></div>
                  </div>
                  <div class="form-group col-sm-6">
                    <label>Time In(Second)</label>
                    <input type="text" id="l_time_in" name="l_time_in" placeholder="" class="form-control " required>
                    <div class="invalid-feedback" id="err_l_time_in"></div>
                  </div>
                  <div class="form-group col-sm-6">
                    <label>Time Out(Second)</label>
                    <input type="text" id="l_time_out" name="l_time_out" placeholder="" class="form-control " required>
                    <div class="invalid-feedback" id="err_l_time_out"></div>
                  </div>
                  <div class="form-group col-sm-6">
                    <label>Late Mins </label>
                    <input type="text" id="late_mins" name="late_mins" placeholder="" class="form-control " required>
                    <div class="invalid-feedback" id="err_late_mins"></div>
                  </div>
                  <div class="form-group col-sm-6">
                    <label>Leave Early Mins </label>
                    <input type="text" id="leave_early_mins" name="leave_early_mins" placeholder="" class="form-control " required>
                    <div class="invalid-feedback" id="err_leave_early_mins"></div>
                  </div>
                  <div class="form-group col-sm-6">
                    <label>Absence Mins </label>
                    <input type="text" id="absence_mins" name="absence_mins" placeholder="" class="form-control " required>
                    <div class="invalid-feedback" id="err_absence_mins"></div>
                  </div>
                   <div class="form-group col-sm-6">
                    <label>Deduction Basis </label>
                    <input type="text" id="deduction_basis" name="deduction_basis" placeholder="" class="form-control " required>
                    <div class="invalid-feedback" id="err_deduction_basis"></div>
                  </div>
                  <div class="form-group col-sm-12">
                    <label>Total </label>
                    <input type="text" id="total" name="total" placeholder="" class="form-control " required>
                    <div class="invalid-feedback" id="err_total"></div>
                  </div>
                  <div class="form-group col-sm-12">
                    <label>Note </label>
                    <textarea  type="text" id="note" name="note" placeholder="" class="form-control " required></textarea>
                    <div class="invalid-feedback" id="err_note"></div>
                  </div>
                  <div class="col-sm-12 text-right">
                    <button class="btn btn-success" type="submit">Submit</button>
                  </div>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              
            </div>
          </div>
        </div>
      </div>

  <script type="text/javascript">
    $("#salry_form").on('submit', function(e){
      var url = $(this).attr('action');
      var mydata = $(this).serialize();
      e.stopPropagation();
      e.preventDefault(e);

       swal({
        title: "Are you sure?",
        text: "Do you want to calculate the attendance report ?",
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        closeOnConfirm: false
        },
          function(){
              $.ajax({
                type:"POST",
                url:url,
                data:mydata,
                cache:false,
                beforeSend:function(){
                    //<!-- your before success function -->
                    loading();
                },
                success:function(response){
                    // console.log(response);
                  if(response.status == true){
                    // console.log(response)
                    swal("Success", response.message, "success");
                    showValidator(response.error,'salry_form');
                  }else{
                    //<!-- your error message or action here! -->
                    showValidator(response.error,'salry_form');
                  }
                },
                error:function(error){
                  console.log(error)
                }
              });
        });
    });



    function load_attendance(){
      let date = $("#date_report").val();
      let print_url = $("#salry_form").attr('data-print');
      var url = main_path + '/attendance/list_attendance/'+date;
      $.ajax({
        type:"GET",
        url:url,
        data:{},
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
          // console.log(response);
         if (response.status == true) {
          if (response.payroll_id != false) {

            $("#pay_id").val(response.payroll_id);
            
            if (response.print_ready == true) {
                $("#btn_print").attr('href', print_url+'/'+response.payroll_id);
                $("#btn_print").removeClass('disable');
                $("#btn_print").attr('target','_blank');
            }else{
                $("#pay_id").val('');
                $("#btn_print").attr('href', "javascript:swal('Info', 'Calculate the payroll first before printing!', 'info');");
                $("#btn_print").addClass('disable');
                $("#btn_print").removeAttr('target','_blank');
            }
          
         
          }else{
            $("#pay_id").val('');
            $("#btn_print").attr('href', "javascript:;");
            $("#btn_print").addClass('disable');
            $("#btn_print").removeAttr('target','_blank');
            // swal("Success", response.message, "success");
          }
         }else{
          console.log(response);
         }
        },
        error: function(error){
          console.log(error);
        }
      });
      
    }

    var datepicker = $('#reservationdate');
    // datepicker.datepicker('setDate', new Date());
      datepicker.datetimepicker({
            format: 'YYYY-MM'
        });

    function upload(){
      close_modal();
      $("#file_upload").modal('show');
    }

    $("#payroll_form").on('submit', function(e){
        e.stopPropagation();
        e.preventDefault(e);

        var url = $(this).attr('action');
        // var mydata = $(this).serialize();
        var form = $('#payroll_form')[0];
        var formData = new FormData(form);

        formData.append('file', $('input[type=file]')[0].files[0]); 

        // console.log(formData);
        

        $.ajax({
          type:"POST",
          url:url,
          data:formData,
          contentType: false,
          processData: false,
          cache:false,
          beforeSend:function(){
              // swal("", "Please Wait..", "");
              // load_waiting("Please wait...","Importing Attendance");
              loading();
          },
          success:function(response){
              // console.log(response);
            if(response.status == true){
              // console.log(response)
              swal("Success", response.message, "success");
              showValidator(response.error,'payroll_form');
              close_modal();
              show_attendance();
              $("#file_upload").modal('hide');
            }else{
              if (response.is_success == 1) {
                swal("Failed", response.message, "error");

              }
              //<!-- your error message or action here! -->
              showValidator(response.error,'payroll_form');
            }
          },
          error:function(error){
            console.log(error)
          }
        });
      });

    function close_modal(){
        $("#filechoose").text('Choose file');
    }


    function previewFile() {
        const file = document.querySelector('input[type=file]').files[0];
        const reader = new FileReader();

        reader.addEventListener("load", function () {
           // console.log(file.name);
           $("#filechoose").text(file.name);
        }, false);

        if (file) {
          reader.readAsDataURL(file);
        }
      }
  </script>


<script type="text/javascript">

$("#sample_form_id").on('submit', function(e){
    var url = $(this).attr('action');
    var mydata = $(this).serialize();
    e.stopPropagation();
    e.preventDefault(e);

    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){
          //<!-- your before success function -->
      },
      success:function(response){
          //console.log(response)
        if(response.status == true){
          console.log(response);
          // show_attendance();
          swal("Success", response.message, "success");
          showValidator(response.error,'sample_form_id');
          $("#modal_edit_attendance").modal('hide');
        }else{
          //<!-- your error message or action here! -->
          showValidator(response.error,'sample_form_id');
        }
      },
      error:function(error){
        console.log(error)
      }
    });
  });


  function delete_attendance(_this){
    var data = JSON.parse($(_this).attr('data-info'));
    var url =  main_path + '/attendance/delete_attendance/' + data.daily_attendance_id;
      swal({
        title: "Are you sure?",
        text: "Do you want to delete this attendance?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        closeOnConfirm: false
      },
      function(){
        $.ajax({
        type:"GET",
        url:url,
        data:{},
        dataType:'json',
        beforeSend:function(){
      },
      success:function(response){
        // console.log(response);
        if (response.status == true) {
          show_attendance();
          swal("Success", response.message, "success");
        }else{
          console.log(response);
        }
      },
      error: function(error){
        console.log(error);
      }
      });
    });
  }


  function edit_attendance(_this){
    var data = JSON.parse($(_this).attr('data-info'));
    $('#daily_attendance_id').val(data.daily_attendance_id);
    $('#attendance_id').val(data.attendance_id);
    $('#f_time_in').val(data.f_time_in);
    $('#f_time_out').val(data.f_time_out);
    $('#l_time_in').val(data.l_time_in);
    $('#l_time_out').val(data.l_time_out);
    $('#late_mins').val(data.late_mins);
    $('#leave_early_mins').val(data.leave_early_mins);
    $('#absence_mins').val(data.absence_mins);
    $('#total').val(data.total);
    $('#note').val(data.note);
    $('#deduction_basis').val(data.deduction_basis);
    $("#modal_edit_attendance").modal('show');
  }


  var tbl_attendance;
  function show_attendance(){
    load_attendance();
    if (tbl_attendance) {
      tbl_attendance.destroy();
    }

    let date = $("#date_report").val();

    var url = main_path + '/attendance/list_attendance/'+date;
    tbl_attendance = $('#tbl_attendance').DataTable({
    pageLength: 25,
    responsive: true,
    ajax: url,
    deferRender: true,
    language: {
    "emptyTable": "No data available"
  },
    columns: [{
    className: '',
    "data": "attendance_id",
    "title": "ID",
  },{
    className: '',
    "data": "users.fullname",
    "title": "Name",
  },{
    className: '',
    "data": "day_week",
    "title": "Date",
  },{
    className: '',
    "data": "time_in_color",
    "title": "Time In(1st)",
  },{
    className: '',
    "data": "f_time_out",
    "title": "Time Out(1st)",
  },{
    className: '',
    "data": "l_time_in",
    "title": "Time In(2nd)",
  },{
    className: '',
    "data": "time_out_color",
    "title": "Time Out(2nd)",
  },{
    className: '',
    "data": "late_mins",
    "title": "Late mins",
  },{
    className: '',
    "data": "late_color",
    "title": "Late",
  },{
    className: 'width-option-1 text-center',
    "data": "daily_attendance_id",
    "orderable": false,
    "title": "Options",
      "render": function(data, type, row, meta){
        var param_data = JSON.stringify(row);
        newdata = '';
        newdata += '<button class="btn btn-success btn-sm font-base mt-1" data-info=\' '+param_data.trim()+'\' onclick="edit_attendance(this)" type="button"><i class="fa fa-edit"></i> Edit</button>';
        newdata += ' <button class="btn btn-danger btn-sm font-base mt-1" data-info=\' '+param_data.trim()+'\' onclick="delete_attendance(this)" type="button"><i class="fa fa-edit"></i> delete</button>';
        return newdata;
      }
    }
  ]
  });

  }

</script>