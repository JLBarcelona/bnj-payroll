<!DOCTYPE html>
<html>
  <!-- Header css meta -->
   @include('Layout.header', ['type' => 'admin', 'title' => 'Employees', 'icon' => asset('img/logo.png') ])
<body class="sidebar-mini layout-fixed" onload="show_holiday();">
  <div class="wrapper">
  <!-- navbar -->
  @include('Layout.nav', ['type' => 'admin'])
  <!-- Sidebar -->
  @include('Layout.sidebar', ['type' => 'admin'])
    <div class="content-wrapper">
      <section class="content">
        <div class="container-fluid">
            <div class="row">
            <div class="col-sm-12 mt-3">
                <div class="card">
                    <div class="card-header h4"><i class="fas fa-calendar"></i> <span>Holiday Settings</span>
						<button class="btn btn-primary btn-sm float-right" onclick="form_reset(); add_holiday()"><i class="fa fa-plus"></i> Add Holiday</button>
               		</div>
					<div class="card-body">
                    <table class="table table-bordered dt-responsive nowrap" id="tbl_holiday" style="width: 100%;"></table>
                    </div>
                    <div class="card-footer"></div>
                </div>
            </div>
            </div>
        </div>
      </section>
    </div>
  </div>
</body>

 <div class="modal fade" role="dialog" id="modal_add_holiday">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <div class="modal-title">
            Add Holiday
            </div>
            <button class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
             <form class="needs-validation" id="holiday_form_id" action="{{ url('/holiday/add_holiday') }}" novalidate>
              <div class="form-row">
                    <input type="hidden" id="holiday_id" name="holiday_id" placeholder="" class="form-control" required>
                    <div class="form-group col-sm-12">
                        <label>Date </label>
                        <input type="date" id="date" name="date" placeholder="" class="form-control " required>
                        <div class="invalid-feedback" id="err_date"></div>
                    </div>
                    <div class="form-group col-sm-12">
                        <label>Description </label>
                        <input type="textarea" id="description" name="description" placeholder="" class="form-control " required>
                        <div class="invalid-feedback" id="err_description"></div>
                    </div>

                  <div class="col-sm-12 text-right">
                    <button class="btn btn-secondary" type="submit">Save</button>
                  </div>
                </div>
              </form>
          </div>
          <div class="modal-footer">

          </div>
        </div>
      </div>
    </div>
  <!-- Footer Scripts -->
  @include('Layout.footer', ['type' => 'admin'])
</html>

<!-- Javascript Function-->
<script>

    function form_reset(){
    $("#holiday_id").val('');
        document.getElementById('holiday_form_id').reset();
    }

    function add_holiday(){
        $("#modal_add_holiday").modal('show');
    }

	var tbl_holiday;
	function show_holiday(){
		if (tbl_holiday) {
			tbl_holiday.destroy();
		}
		var url = main_path + '/holiday/list_holiday';
		tbl_holiday = $('#tbl_holiday').DataTable({
		pageLength: 10,
		responsive: true,
		ajax: url,
		deferRender: true,
		language: {
		"emptyTable": "No data available"
	},
		columns: [{
		className: '',
		"data": "date",
		"title": "Date",
	},{
		className: '',
		"data": "description",
		"title": "Description",
	},{
		className: 'width-option-1 text-center',
		"data": "holiday_id",
		"orderable": false,
		"title": "Options",
			"render": function(data, type, row, meta){
				var param_data = JSON.stringify(row);
				newdata = '';
				newdata += '<button class="btn btn-success btn-sm font-base mt-1" data-info=\' '+param_data.trim()+'\' onclick="edit_holiday(this)" type="button"><i class="fa fa-edit"></i> Edit</button>';
				newdata += ' <button class="btn btn-danger btn-sm font-base mt-1" data-info=\' '+param_data.trim()+'\' onclick="delete_holiday(this)" type="button"><i class="fa fa-edit"></i> Delete</button>';
				return newdata;
			}
		}
	]
	});
	}

	$("#holiday_form_id").on('submit', function(e){
		var url = $(this).attr('action');
		var mydata = $(this).serialize();
		e.stopPropagation();
		e.preventDefault(e);

		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
			},
			success:function(response){
					//console.log(response)
				if(response.status == true){
					console.log(response)
					swal("Success", response.message, "success");
					showValidator(response.error,'holiday_form_id');
					$('#modal_add_holiday').modal('hide');
					$('body').removeClass('modal-open');
					$('.modal-backdrop').remove();
					show_holiday();
				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'holiday_form_id');
				}
			},
			error:function(error){
				console.log(error)
			}
		});
	});

	function delete_holiday(_this){
		var data = JSON.parse($(_this).attr('data-info'));
		var url =  main_path + '/holiday/delete_holiday/' + data.holiday_id;
			swal({
				title: "Are you sure?",
				text: "Do you want to delete this holiday?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes",
				closeOnConfirm: false
			},
			function(){
				$.ajax({
				type:"GET",
				url:url,
				data:{},
				dataType:'json',
				beforeSend:function(){
			},
			success:function(response){
				// console.log(response);
				if (response.status == true) {
					swal("Success", response.message, "success");
					show_holiday();
				}else{
					console.log(response);
				}
			},
			error: function(error){
				console.log(error);
			}
			});
		});
	}

	function edit_holiday(_this){
		var data = JSON.parse($(_this).attr('data-info'));
		$('#holiday_id').val(data.holiday_id);
		$('#date').val(data.date);
		$('#description').val(data.description);
		$("#modal_add_holiday").modal('show');
	}
</script>

