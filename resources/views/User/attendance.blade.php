<!DOCTYPE html>
<html>
  <!-- Header css meta -->
   @include('Layout.header', ['type' => 'user', 'title' => 'User', 'icon' => asset('img/logo.png') ])
   <link rel="stylesheet" href="{{ asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
<body class="sidebar-mini layout-fixed" onload="show_attendance(); ">
  <div class="wrapper">
  <!-- navbar -->
  @include('Layout.nav', ['type' => 'user'])
  <!-- Sidebar -->
  @include('Layout.sidebar', ['type' => 'user'])
    <div class="content-wrapper">
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-12 mt-3">
              <div class="card">
                <div class="card-header h4"><i class="fas fa-calendar"></i> <span>Attendance</span>
                </div>
                <div class="card-body">
                  <form class="needs-validation" id="salry_form" action="" novalidate data-print="{{ url('user/print/payslip') }}">
                    <input type="hidden" name="pay_id" id="pay_id">
                    <div class="card ">
                      <div class="card-body">
                        <div class="form-row">
                          <div class="col-sm-5"></div>
                          <div class="col-sm">
                            <div class="input-group date" id="reservationdate" data-target-input="nearest">
                              <div class="input-group-prepend" data-target="#reservationdate" data-toggle="datetimepicker">
                                <div class="input-group-text">Date</div>
                              </div>
                              <input type="text"  id="date_report" name="date_report" value="" class="form-control datetimepicker-input" data-target="#reservationdate">
                              <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                              </div>
                            <div class="invalid-feedback" id="err_date_report"></div>
                            </div>
                          </div>
                          <div class="col-sm-1">
                            <div class="form-group">
                              <button class="btn btn-block btn-success" type="button" onclick="show_attendance();"><i class="fa fa-search"></i></button>
                            </div>
                          </div>
                           <div class="col-sm-2">
                              <div class="form-group">
                                <a href="" id="btn_print" class="btn btn-block btn-info disable"><i class="fa fa-print"></i> <small>Print Payslip </small></a>
                              </div>
                            </div>
                          <!--       <div class="col-sm">
                            <div class="form-group">
                              <button class="btn btn-block btn-primary"></button>
                            </div>
                          </div> -->
                        </div>
                      </div>
                    </div>
                  </form>
                  <table class="table table-bordered dt-responsive nowrap" id="tbl_attendance" style="width: 100%;"></table>

                </div>
                <div class="card-footer"></div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</body>
  <!-- Footer Scripts -->
  @include('Layout.footer', ['type' => 'user'])
  <script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
  <script src="{{ asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
</html>

<script type="text/javascript">
var tbl_attendance;

var datepicker = $('#reservationdate');
// datepicker.datepicker('setDate', new Date());
  datepicker.datetimepicker({
        format: 'YYYY-MM'
    });


function load_attendance(){
      let date = $("#date_report").val();
      let print_url = $("#salry_form").attr('data-print');
      var url = main_path + '/user/attendance/list_attendance/'+date;
      $.ajax({
        type:"GET",
        url:url,
        data:{},
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
          console.log(response);
         if (response.status == true) {
          if (response.payroll_id != false) {

            $("#pay_id").val(response.payroll_id);

            if (response.print_ready == true) {
                $("#btn_print").attr('href', print_url+'/'+response.payroll_id);
                $("#btn_print").removeClass('disable');
                $("#btn_print").attr('target','_blank');
            }else{
                $("#pay_id").val('');
                $("#btn_print").attr('href', "javascript:swal('Info', 'Calculate the payroll first before printing!', 'info');");
                $("#btn_print").addClass('disable');
                $("#btn_print").removeAttr('target','_blank');
            }


          }else{
            $("#pay_id").val('');
            $("#btn_print").attr('href', "javascript:;");
            $("#btn_print").addClass('disable');
            $("#btn_print").removeAttr('target','_blank');
            // swal("Success", response.message, "success");
          }
         }else{
          console.log(response);
         }
        },
        error: function(error){
          console.log(error);
        }
      });

    }

  function show_attendance(){
    load_attendance();
    if (tbl_attendance) {
      tbl_attendance.destroy();
    }

    let date = $("#date_report").val();

    var url = main_path + '/user/attendance/list_attendance/'+date;
    tbl_attendance = $('#tbl_attendance').DataTable({
    pageLength: 25,
    responsive: true,
    ajax: url,
    deferRender: true,
    language: {
    "emptyTable": "No data available"
  },
    columns: [{
    className: '',
    "data": "users.fullname",
    "title": "Name",
  },{
    className: '',
    "data": "day_week",
    "title": "Date",
  },{
    className: '',
    "data": "time_in_color",
    "title": "Time In(1st)",
  },{
    className: '',
    "data": "f_time_out",
    "title": "Time Out(1st)",
  },{
    className: '',
    "data": "l_time_in",
    "title": "Time In(2nd)",
  },{
    className: '',
    "data": "time_out_color",
    "title": "Time Out(2nd)",
  },{
    className: '',
    "data": "late_mins",
    "title": "Late mins",
  },{
    className: '',
    "data": "late_color",
    "title": "Late",
  }
  ]
  });

  }


</script>
