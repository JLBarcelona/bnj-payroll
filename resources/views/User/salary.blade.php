<!DOCTYPE html>
<html>
  <!-- Header css meta -->
   @include('Layout.header', ['type' => 'user', 'title' => 'User', 'icon' => asset('img/logo.png') ])
   <link rel="stylesheet" href="{{ asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
<body class="sidebar-mini layout-fixed" onload="show_salary(); ">
  <div class="wrapper">
  <!-- navbar -->
  @include('Layout.nav', ['type' => 'user'])
  <!-- Sidebar -->
  @include('Layout.sidebar', ['type' => 'user'])
    <div class="content-wrapper">
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-12 mt-3">
              <div class="card">
                <div class="card-header h4"><i class="fas fa-money-check-alt"></i> <span>Salary</span>
                </div>
                <div class="card-body">
                  <table class="table table-bordered dt-responsive nowrap" id="tbl_salary" style="width: 100%;"></table>
                </div>
                <div class="card-footer"></div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</body>
  <!-- Footer Scripts -->
  @include('Layout.footer', ['type' => 'user'])
  <script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
  <script src="{{ asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
</html>

<script type="text/javascript">
var datepicker = $('#reservationdate');
// datepicker.datepicker('setDate', new Date());
  datepicker.datetimepicker({
        format: 'YYYY-MM'
    });

var tbl_salary;

    function load_salary(){
          let date = $("#date_report").val();
          let print_url = $("#salry_form").attr('data-print');
          var url = main_path + '/user/salary/list_salary/'+date;
          $.ajax({
            type:"GET",
            url:url,
            data:{},
            dataType:'json',
            beforeSend:function(){
            },
            success:function(response){
              console.log(response);
             if (response.status == true) {
              if (response.payroll_id != false) {

                $("#pay_id").val(response.payroll_id);

                // if (response.print_ready == true) {
                //     $("#btn_print").attr('href', print_url+'/'+response.payroll_id);
                //     $("#btn_print").removeClass('disable');
                //     $("#btn_print").attr('target','_blank');
                // }else{
                //     $("#pay_id").val('');
                //     $("#btn_print").attr('href', "javascript:swal('Info', 'Calculate the payroll first before printing!', 'info');");
                //     $("#btn_print").addClass('disable');
                //     $("#btn_print").removeAttr('target','_blank');
                // }


              }else{
                // $("#pay_id").val('');
                // $("#btn_print").attr('href', "javascript:;");
                // $("#btn_print").addClass('disable');
                // $("#btn_print").removeAttr('target','_blank');
                // swal("Success", response.message, "success");
              }
             }else{
              console.log(response);
             }
            },
            error: function(error){
              console.log(error);
            }
          });

        }


        function show_salary(){
          load_salary();
          if (tbl_salary) {
            tbl_salary.destroy();
          }

          let date = $("#date_report").val();

          var url = main_path + '/user/salary/list_salary/'+date;
          tbl_salary = $('#tbl_salary').DataTable({
          pageLength: 25,
          responsive: true,
          ajax: url,
          deferRender: true,
          language: {
          "emptyTable": "No data available"
        },
          columns: [{
          className: '',
          "data": "payroll_date",
          "title": "Date",
        },{
          className: '',
          "data": "per_hour",
          "title": "Per Hour",
        },{
          className: '',
          "data": "per_day",
          "title": "Per Day",
        },{
          className: '',
          "data": "holiday_work",
          "title": "Hodilay",
        },{
          className: '',
          "data": "holiday_salary",
          "title": "Holiday Salary",
        },{
          className: '',
          "data": "total_late",
          "title": "Total Late",
        },{
          className: '',
          "data": "ot_ab",
          "title": "Overtime/Absent",
        },{
          className: '',
          "data": "salary",
          "title": "Salary",
        },{
          className: '',
          "data": "days_working",
          "title": "Days",
        }
        ]
        });

        }

</script>
