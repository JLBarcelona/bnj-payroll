<!DOCTYPE html>
<html>
  <!-- Header css meta -->
   @include('Layout.header', ['type' => 'user', 'title' => 'User', 'icon' => asset('img/logo.png') ])
<body class="sidebar-mini layout-fixed">
  <div class="wrapper">
  <!-- navbar -->
  @include('Layout.nav', ['type' => 'user'])
  <!-- Sidebar -->
  @include('Layout.sidebar', ['type' => 'user'])
   <div class="content-wrapper">
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</body>
  <!-- Footer Scripts -->
  @include('Layout.footer', ['type' => 'user'])
</html>