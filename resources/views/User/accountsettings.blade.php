<!DOCTYPE html>
<html>
  <!-- Header css meta -->
   @include('Layout.header', ['type' => 'user', 'title' => 'Account Settings', 'icon' => asset('img/logo.png') ])
<body class="sidebar-mini layout-fixed">
  <div class="wrapper">
  <!-- navbar -->
  @include('Layout.nav', ['type' => 'user'])
  <!-- Sidebar -->
  @include('Layout.sidebar', ['type' => 'user'])
    <div class="content-wrapper">
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12 mt-3">
              <div class="card">
                <div class="card-header bg-light ">
					<div class="card-header h4"><i class="fas fa-cog"></i> <span>Account Settings</span></div>
                </div>
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="card">
                        <div class="card-header bg-light">
                          Profile
                        </div>
                        <ul class="list-group list-group-flush">
                          <li class="list-group-item">Attendance ID <span class="float-right">{{ Auth::user()->attendace_id }}</span></li>
                          <li class="list-group-item">Name <span class="float-right">{{ Auth::user()->fullname }}</span></li>
                          <li class="list-group-item">Username <span class="float-right">{{ Auth::user()->username }}</span></li>
                          <li class="list-group-item">Date Registered <span class="float-right">{{ \Carbon\Carbon::parse(Auth::user()->created_at)->format('M d, Y')}}</span></li>
                        </ul>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="card">
                        <div class="card-header bg-light">
                          Update
                        </div>
                        <div class="card-body">
                          <form class="needs-validation" id="user_settings" action="{{ url('user/add_user') }}" novalidate>
                          	<div class="form-row">
                          		<input type="hidden" id="user_id" name="user_id" value="{{ Auth::user()->user_id }}" placeholder="" class="form-control" required>
                              <div class="form-group col-sm-12">
                          			<label>Firstname </label>
                          			<input type="text" id="firstname" name="firstname" placeholder="Firstname" class="form-control " value="{{ Auth::user()->firstname }}" required>
                          			<div class="invalid-feedback" id="err_firstname"></div>
                          		</div>
                          		<div class="form-group col-sm-12">
                          			<label>Middlename </label>
                          			<input type="text" id="middlename" name="middlename" placeholder="Middlename" class="form-control " value="{{ Auth::user()->middlename }}" required>
                          			<div class="invalid-feedback" id="err_middlename"></div>
                          		</div>
                          		<div class="form-group col-sm-12">
                          			<label>Lastname </label>
                          			<input type="text" id="lastname" name="lastname" placeholder="Lastname" class="form-control " value="{{ Auth::user()->lastname }}" required>
                          			<div class="invalid-feedback" id="err_lastname"></div>
                          		</div>
                          		<div class="form-group col-sm-12">
                          			<label>Username </label>
                          			<input type="text" id="username" name="username" placeholder="Username" class="form-control " value="{{ Auth::user()->username }}" required>
                          			<div class="invalid-feedback" id="err_username"></div>
                          		</div>
                          		<div class="form-group col-sm-12">
                          			<label>Email Address </label>
                          			<input type="text" id="email_address" name="email_address" placeholder="Email Address" class="form-control " value="{{ Auth::user()->email_address }}" required>
                          			<div class="invalid-feedback" id="err_email_address"></div>
                          		</div>
                          		<div class="col-sm-12 text-right">
                                <button class="btn btn-secondary" type="submit">Save</button>
                                <button type="button" class="btn btn-outline-secondary" onclick="change_password()">Change Password</button>
                          		</div>
                          	</div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card-footer"></div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>

    <div class="modal fade" role="dialog" id="modal_change_password">
         <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header bg-light">
               <div class="modal-title">
               Change Password
               </div>
               <button class="close" data-dismiss="modal">&times;</button>
             </div>
             <div class="modal-body">
               <form class="needs-validation" id="change_password" action="{{ url('user/changepassword') }}" novalidate>
                	<div class="form-row">
                		<input type="hidden" id="user_id" name="user_id" value="{{ Auth::user()->user_id }}" placeholder="" class="form-control" required>
                		<div class="form-group col-sm-12">
                			<label>Current Password </label>
                			<input type="text" id="currentpassword" name="currentpassword" placeholder="Current Password" class="form-control " required>
                			<div class="invalid-feedback" id="err_currentpassword"></div>
                		</div>
                		<div class="form-group col-sm-12">
                			<label>New Password </label>
                			<input type="password" id="newpassword" name="newpassword" placeholder="New Password" class="form-control " required>
                			<div class="invalid-feedback" id="err_newpassword"></div>
                		</div>
                		<div class="form-group col-sm-12">
                			<label>Confirm Password </label>
                			<input type="password" id="confirmpassword" name="confirmpassword" placeholder="Confirm Password" class="form-control " required>
                			<div class="invalid-feedback" id="err_confirmpassword"></div>
                		</div>

                		<div class="col-sm-12 text-right">
                			<button class="btn btn-secondary" type="submit">Save</button>
                		</div>
                	</div>
                </form>
             </div>
           </div>
         </div>
       </div>
  </div>
</body>
  <!-- Footer Scripts -->
  @include('Layout.footer', ['type' => 'user'])
</html>

<script type="text/javascript">
  function change_password(){
    $("#modal_change_password").modal('show');
  }
</script>

<script>
	var tbl_user;
	function show_user(){
		if (tbl_user) {
			tbl_user.destroy();
		}
		var url = main_path + '/user/list_user';
		tbl_user = $('#tbl_user').DataTable({
		pageLength: 10,
		responsive: true,
		ajax: url,
		deferRender: true,
		language: {
		"emptyTable": "No data available"
	},
		columns: [{
		className: '',
		"data": "firstname",
		"title": "Firstname",
	},{
		className: '',
		"data": "middlename",
		"title": "Middlename",
	},{
		className: '',
		"data": "lastname",
		"title": "Lastname",
	},{
		className: '',
		"data": "username",
		"title": "Username",
	},{
		className: '',
		"data": "email_address",
		"title": "Email_address",
	},{
		className: 'width-option-1 text-center',
		"data": "user_id",
		"orderable": false,
		"title": "Options",
			"render": function(data, type, row, meta){
				var param_data = JSON.stringify(row);
				newdata = '';
				newdata += '<button class="btn btn-success btn-sm font-base mt-1" data-info=\' '+param_data.trim()+'\' onclick="edit_user(this)" type="button"><i class="fa fa-edit"></i> Edit</button>';
				newdata += '<button class="btn btn-danger btn-sm font-base mt-1" data-info=\' '+param_data.trim()+'\' onclick="delete_user(this)" type="button"><i class="fa fa-edit"></i> delete</button>';
				return newdata;
			}
		}
	]
	});
	}

	$("#user_settings").on('submit', function(e){
		var url = $(this).attr('action');
		var mydata = $(this).serialize();
		e.stopPropagation();
		e.preventDefault(e);

		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
			},
			success:function(response){
					//console.log(response)
				if(response.status == true){
					console.log(response)
					swal("Success", response.message, "success");
					showValidator(response.error,'user_settings');
				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'user_settings');
				}
			},
			error:function(error){
				console.log(error)
			}
		});
	});

	function delete_user(_this){
		var data = JSON.parse($(_this).attr('data-info'));
		var url =  main_path + '/user/delete_user/' + data.user_id;
			swal({
				title: "Are you sure?",
				text: "Do you want to delete this user?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes",
				closeOnConfirm: false
			},
			function(){
				$.ajax({
				type:"GET",
				url:url,
				data:{},
				dataType:'json',
				beforeSend:function(){
			},
			success:function(response){
				// console.log(response);
				if (response.status == true) {
					swal("Success", response.message, "success");
				}else{
					console.log(response);
				}
			},
			error: function(error){
				console.log(error);
			}
			});
		});
	}

	function edit_user(_this){
		var data = JSON.parse($(_this).attr('data-info'));
		$('#user_id').val(data.user_id);
		$('#firstname').val(data.firstname);
		$('#middlename').val(data.middlename);
		$('#lastname').val(data.lastname);
		$('#username').val(data.username);
		$('#email_address').val(data.email_address);
	}

  $("#change_password").on('submit', function(e){
		var url = $(this).attr('action');
		var mydata = $(this).serialize();
		e.stopPropagation();
		e.preventDefault(e);

		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
			},
			success:function(response){
					//console.log(response)
				if(response.status == true){
					console.log(response)
					swal("Success", response.message, "success");
					showValidator(response.error,'change_password');
					$('#modal_change_password').modal('hide');
					$('body').removeClass('modal-open');
					$('.modal-backdrop').remove();
				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'change_password');
				}
			},
			error:function(error){
				console.log(error)
			}
		});
	});

</script>
