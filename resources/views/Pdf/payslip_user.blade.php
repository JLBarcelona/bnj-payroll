<style>
	tr > td{
		font-size:10px;
	}.title{
		font-size:9px;
		font-weight: bold;
	}.data{
		font-size:9px;
	}.head-title{
		font-size: 11px;
		font-weight: bold;
	}.head-title-date{
		font-size: 10px;
		font-weight: bold;
	}.sub-title{
		font-size: 9px;
	}
	.margin{
		margin-bottom: 10px;
	}
	.bg-info{
		background-color: #17a2b8;
		color: #fff;
	}
	.bg-success{
		background-color: #28a745;
		color: #fff;
	}
	.bg-primary{
		background-color: #007bff;
		color: #fff;
	}.bg-dark{
		background-color: #555;
		color: #fff;
	}.italic{
		font-style: italic;
		font-size: 8px;
		color: #a1a1a1;
	}.bold{
		font-weight: bold;
	}.bg-dim{
		background-color: #a5a5a5;
		color: #fff;
	}
</style>

@foreach($data->salary->where('owner_id', $user_id) as $datas)
<table border="1" width="100%" cellpadding="6" class="margin">
	<thead>
		<tr class="">
			<td class="logo" align="center">
				<!-- <img src="{{ asset('img/logo.png') }}" alt=""> -->
				<br>
				<span class="head-title">Brain Network Japan</span>
				<!-- <br> -->
				<!-- <small class="sub-title">Brain Network Japan</small> -->
				<br>
				<small class="italic">Proof that Salary Received</small>
				<br>
				<small class="head-title-date">Salary for {{ \Carbon\Carbon::parse($data->report_from)->format('F Y') }}</small>
				<br>
				<small class="head-title-date">Period: {{ $data->report_from }} ~ {{ $data->report_to }}</small>
			</td>
		</tr>
	</thead>
	<tbody>
		<tr class="">
			<td class="title bg-dark" align="left" width="40%">Name of Employee</td>
			<td class="title bg-dark" align="left" width="20%">Salary</td>
			<td class="title bg-dark" align="left" width="20%">Signature</td>
			<td class="title bg-dark" align="left" width="20%">Date Received</td>
		</tr>
		<tr class="">
			<td class="data" align="left" width="40%">{{ $datas->owner->fullname }}</td>
			<td class="data" align="left" width="20%">{{ env('APP_CURRENCY') }} {{ number_format($datas->salary) }}</td>
			<td class="data" align="left" width="20%"></td>
			<td class="data" align="left" width="20%"></td>
		</tr>
	</tbody>
</table>

<table width="100%" cellpadding="1" class="margin">
	<tbody>
		<tr><td width="100%"></td></tr>
	</tbody>
</table>
@endforeach