<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $primaryKey = 'user_id';


    protected $table = 'bnj_user';

    protected $fillable = [
        'attendance_id','profile_picture','firstname', 'lastname', 'middlename', 'password', 'username', 'email_address', 'gender', 'birthdate', 'address', 'country', 'postal_code', 'user_type', 'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = ['fullname', 'avatar'];


    public function getAvatarAttribute(){
        if (!empty($this->profile_picture)) {
            return 'profile_picture/'.$this->profile_picture;
        }else{
            return 'img/avatar.png';
        }
    }

    public function salaryconfig()
    {
        return $this->morphOne('App\SalaryConfig', 'owner')->whereNull('deleted_at');
    }

    public function lateconfig()
    {
        return $this->morphMany('App\LateConfig', 'owner')->whereNull('deleted_at');
    }

    public function benefit()
    {
        return $this->morphOne('App\Benefit', 'owner')->whereNull('deleted_at');
    }

    public function attendance()
    {
        return $this->hasMany('App\DailyAttendance', 'attendance_id');
    }

    public function salary()
    {
        return $this->morphMany('App\Salary', 'owner')->whereNull('deleted_at');

    }

    public function chatmember()
    {
        return $this->morphMany('App\ChatMember', 'owner')->whereNull('deleted_at');
    }

    public function chat()
    {
        return $this->morphMany('App\Chat', 'owner')->whereNull('deleted_at');
    }

    public function chatmessage()
    {
        return $this->morphMany('App\ChatDetail', 'owner')->whereNull('deleted_at');
    }


    public function setPasswordAttribute($value){
        $this->attributes['password'] = Hash::make($value);
    }

    public function getFullnameAttribute(){
         return $this->firstname.' '.$this->lastname;
    }
}
