<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Excuses extends Model
{
  protected $primaryKey = 'exception_id';

  protected $table = 'bnj_excuses';

  protected $fillable = [
      'payroll_id', 'payroll_type', 'owner_id', 'owner_type', 'attendance_id','date','time_in','time_out','note','created_at','updated_at','deleted_at'
      ];
}
