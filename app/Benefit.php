<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Benefit extends Model
{
    protected $primaryKey = 'benifit_id';


    protected $table = 'bnj_benifits';

    protected $fillable = [
       'sss_number', 'phil_health_number', 'pag_ibig_number', 'sss_share', 'phil_health_share', 'pag_ibig_share', 'created_at', 'updated_at', 'deleted_at'
    ];

    protected $hidden = ['owner_id', 'owner_type'];

     public function owner()
    {
        return $this->morphTo();
    }

}
