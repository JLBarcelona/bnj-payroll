<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payroll extends Model
{
  protected $primaryKey = 'payroll_id';

  protected $table = 'bnj_payroll';

  protected $fillable = [
      'report_from', 'report_to', 'created_at', 'modified_at', 'deleted_at'
      ];


	public function attendance()
	{
		return $this->morphMany('App\DailyAttendance', 'owner')->whereNull('deleted_at');
	}

	public function detail()
	{
		return $this->morphMany('App\PayrollDetail', 'owner')->whereNull('deleted_at');
	}

	public function salary()
	{
		return $this->morphMany('App\Salary', 'payroll')->whereNull('deleted_at');
	}
}
