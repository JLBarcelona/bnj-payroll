<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LateConfig extends Model
{
  protected $primaryKey = 'late_config_id';

  protected $table = 'bnj_late_config';

  protected $fillable = [
      'from_time','end_time','deducted_from_time','owner_id','owner_type','created_at','updated_at','deleted_at'
      ];

      protected $hidden = ['owner_id', 'owner_type'];

      public function owner()
        {
            return $this->morphTo();
        }
}
