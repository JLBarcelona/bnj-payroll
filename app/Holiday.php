<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Holiday extends Model
{
	protected $primaryKey = 'holiday_id';

	protected $table = 'bnj_holiday';

	protected $fillable = [
	'date','description','owner_id','owner_type','created_at','updated_at','deleted_at'
	];

	protected $hidden = ['owner_id', 'owner_type'];

	public function owner()
	{
	    return $this->morphTo();
	}
}
