<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Validator;


use App\User;
use App\Benefit;
use App\Payroll;
use App\DailyAttendance;

use App\Salary;

// Excel
use App\Imports\AttendanceImport;
use Maatwebsite\Excel\Facades\Excel;

use Str;
use Carbon\Carbon;


use PDF;

class ReportController extends Controller
{

	function print_payslip($pay_id){
		$user_id = Auth::user()->user_id;


		$data = Payroll::find($pay_id);
		$date_1 = $data->report_from;
		$date_2 = $data->report_to;

		$logo = asset('img/logo.png');

		$view = \View::make('Pdf.payslip_user', compact('data', 'logo', 'user_id'));
		$html_content = $view->render();

		PDF::SetTitle('Salary for '. $date_1.' ~ '.$date_2);
		PDF::AddPage();
		PDF::writeHTML($html_content, true, false, true, false, '');
		PDF::Output(uniqid().'payroll-'.$date_1.'-'.$date_2.'.pdf');
	}

    function list_attendance_user($date = ''){
    	$user = Auth::user();
		$date_1 = Carbon::parse($date.'-1')->format('Y-m-d');
		$date_2 = Carbon::parse($date_1)->endOfMonth()->format('Y-m-d');

		$payroll = Payroll::where('report_from', $date_1)->Where('report_to', $date_2);

		if ($payroll->count() > 0) {
			$pay = $payroll->first();
			$detail = DailyAttendance::where('owner_id', $pay->payroll_id)->where('attendance_id', $user->attendance_id)->get();

			$print_check = Salary::where('payroll_id', $pay->payroll_id);

			if ($print_check->count() > 0) {
				return response()->json(['status' => true, 'data' => $detail, 'payroll_id' => $pay->payroll_id, 'print_ready' => true]);
			}else{
				return response()->json(['status' => true, 'data' => $detail, 'payroll_id' => $pay->payroll_id , 'print_ready' => false]);
			}

		}else{
			return response()->json(['status' => true, 'data' => [],  'payroll_id' => false ]);
		}
	}



	function list_salary_user($date = ''){
		$user = Auth::user();
		$detail = $user->salary;
		return response()->json(['status' => true, 'data' => $detail]);
		
		// $date_1 = Carbon::parse($date.'-1')->format('Y-m-d');
		// $date_2 = Carbon::parse($date_1)->endOfMonth()->format('Y-m-d');

		// $payroll = Payroll::where('report_from', $date_1)->Where('report_to', $date_2);

		// if ($payroll->count() > 0) {
		// 	$pay = $payroll->first();
		// 	$detail = Salary::where('payroll_id', $pay->payroll_id)->where('user_id', $user->user_id)->get();
		// 	// if ($print_check->count() > 0) {
		// 	// 	return response()->json(['status' => true, 'data' => $detail, 'payroll_id' => $pay->payroll_id, 'print_ready' => true]);
		// 	// }else{
		// 	// 	return response()->json(['status' => true, 'data' => $detail, 'payroll_id' => $pay->payroll_id , 'print_ready' => false]);
		// 	// }

		// }else{
		// 	return response()->json(['status' => true, 'data' => [],  'payroll_id' => false ]);
		// }
	}


}
