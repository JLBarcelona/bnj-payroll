<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Validator;

use App\User;

class AccountController extends Controller
{
  function verify_account(Request $request, $token, $id){
    $check = User::where('verification_code', $token)->where('user_id', $id)->whereNull('verified_at');
    if ($check->count() > 0) {
      $verify = $check->first();
      $verify->verified_at = now();

      if ($verify->save()) {
        return view('Account.verified', compact('verify'));
      }

    }else{
        return view('Account.login');
    }
  }



   function login_user(Request $request){

    	$result = array();


    	$user_login = array(
    		'email_address' => $request->get('email_address'),
    		'password' => $request->get('password')
    	);


    		$validator = Validator::make($request->all(), [
              'email_address' => 'required',
              'password' => 'required',
        ]);



		if ($validator->fails()) {
			 return response()->json(['status' => false, 'error' => $validator->errors()]);
		}else{
		
	      	if (Auth::attempt($user_login)) {
	         
	          $user_type = Auth::user()->user_type;
            $verified = Auth::user()->is_verified;

            if (!empty($verified) || $verified !== 0) {
              return response()->json(['status' => true, 'message' => 'Accept!', 'user_type' => $user_type]);
            }else{
              $validator->errors()->add('email_address', 'Please verify your account to your email!');
              // $validator->errors()->add('password', 'Please verify your  to your email!');
              return response()->json(['status' => false, 'error' => $validator->errors()]);
            }
	      	
          }else{
	            $validator->errors()->add('email_address', 'Invalid Account!');
	            $validator->errors()->add('password', 'Invalid Account!');
	            return response()->json(['status' => false, 'error' => $validator->errors()]);
	      }
	}

  }

    function logout_admin(){
        auth()->guard('admin')->logout();
        return redirect('/');
    }

    function logout_user(){
        Auth::logout();
        return redirect('/');
    }

     public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return redirect()->intended('/');
        }
    }
}
