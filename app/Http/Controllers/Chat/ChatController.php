<?php

namespace App\Http\Controllers\Chat;

use App\Events\PusherChat;

use App\Chat;
use App\ChatDetail;
use App\ChatMember;
use App\User;

use Auth;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ChatController extends Controller
{

    function chat($chat_id = ''){
        $user_id = Auth::user()->user_id;

        $users = User::whereNull('deleted_at')->where('user_id', '!=', $user_id)->get();
        $chat = '';
        $chats = Chat::where('chat_id', $chat_id);
        if ($chats->count() > 0) {
           $chat = $chats->first();
           $c_id = $chat->chat_id;

        }else{
            $chat = '';
        }

        return view('Chat.chat', compact('chat', 'users'));
    }


    function chat_list($filter = ''){
        $user = Auth::user();
        $data = '';
        if (!empty($filter)) {
            $datas = $user->chatmember;
        }else{
            $datas = $user->chatmember;
        }

        foreach ($datas as $val) {
            $new = ($val->chat->is_seen > 0) ? 'new text-dark' : 'old';

            $data .= '<li class="'.$new.'">';
            $data .= '<a href="'.url('m/chat/'.$val->chat_id).'">';
            $data .= '<img class="contacts-list-img" src="'.asset($val->owner->avatar).'">';
            $data .= '<div class="contacts-list-info">';
            $data .= '<span class="contacts-list-name text-dark">';
            $data .=  $val->group_name;
            $data .= '<small class="contacts-list-date float-right">1/4/2015</small>';
            $data .= '</span>';
            $data .= '<span class="contacts-list-msg d-inline-block text-truncate '.$new.'" style="max-width: 150px;">'.$val->chat->last_message.'</span>';
            $data .= '</div>';
            $data .= '</a>';
            $data .= '</li>';
        }

        return response()->json(['status' => true, 'data' => $data]);
        // return response()->json(['status' => true, 'data' => $datas]);
    }

    function send_message(Request $request){
        $chat_id_box = $request->get('chat_id_box');
        $user_receipients = $request->get('user_receipients');
        $message = $request->get('message'); 

        if (!empty($chat_id_box)) {
           $chat_id = Chat::where('chat_id');
        }else{
            if (count($user_receipients) > 1) {
               foreach ($user_receipients as $val) {
                    $user = User::where('user_id', $val);
                    if ($user->count() > 0) {
                        $_user = $user->first();
                        $check_chat = ChatMember::where('owner_id', $_user->user_id)->where('is_group', 0);
                        if ($check_chat->count()) {
                            
                        }
                    }else{
                        
                    }
                }
            }else{
                foreach ($user_receipients as $val) {
                    $user = User::where('user_id', $val);

                    if ($user->count() > 0) {
                        $_user = $user->first();
                        $check_chat = ChatMember::where('owner_id', $_user->user_id)->where('is_group', 0);
                        if ($check_chat->count()) {
                            
                        }

                    }else{

                    }
                }
            }
        }

        

        

        // $message = $request->get('message');
    	// $type = 'send_message';
    	// $data = ['sender' => 'JL', 'receiver' => 'Raven'];

		event(new PusherChat($message, $data, $type));
    }


    function get_chat(Request $request){
    	$user = Auth()->user();
    	$owner_id = $user->user_id;
    	$owner_type = get_class($user);

    	$receiver_id = $request->get('user_id');
    	
    	$check = Chat::where('receiver_id', $receiver_id);

    	if ($check->count() > 0) {
    		$chats = $check->first();
			return response()->json(['status' => true, 'message' => 'Chat save success!', 'receiver_id' => $chats->receiver_id, 'chat_id' => $chats->chat_id]);
    	}else{
    		$users = User::where('user_id', $receiver_id);
			if ($users->count() > 0) {
				$chat = new Chat;
				$chat->owner_id = $owner_id;
	    		$chat->owner_type = $owner_type;
	    		$chat->receiver_id = $receiver_id;
	    		$chat->receiver_type = get_class($users->first());

	    		if ($chat->save()) {
					return response()->json(['status' => true, 'message' => 'Chat save success!', 'receiver_id' => $chat->receiver_id, 'chat_id' => $chat->chat_id]);
	    		}

			}else{
				return response()->json(['status' => false, 'message' => 'user not found!']);
			}
    		
    	}

    }
}
