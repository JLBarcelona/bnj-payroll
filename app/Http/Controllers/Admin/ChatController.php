<?php

namespace App\Http\Controllers\Admin;

use App\Events\PusherChat;

use App\Chat;
use App\ChatDetail;
use App\User;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    function send_message(Request $request){
    	$message = $request->get('message');
    	$type = 'send_message';
    	$data = ['sender' => 'JL', 'receiver' => 'Raven'];

		event(new PusherChat($message, $data, $type));
    }


    function get_chat(Request $request){
    	$user = Auth()->user();
    	$owner_id = $user->user_id;
    	$owner_type = get_class($user);

    	$receiver_id = $request->get('user_id');
    	
    	$check = Chat::where('receiver_id', $receiver_id);

    	if ($check->count() > 0) {
    		$chats = $check->first();
			return response()->json(['status' => true, 'message' => 'Chat save success!', 'receiver_id' => $chats->receiver_id, 'chat_id' => $chats->chat_id]);
    	}else{
    		$users = User::where('user_id', $receiver_id);
			if ($users->count() > 0) {
				$chat = new Chat;
				$chat->owner_id = $owner_id;
	    		$chat->owner_type = $owner_type;
	    		$chat->receiver_id = $receiver_id;
	    		$chat->receiver_type = get_class($users->first());

	    		if ($chat->save()) {
					return response()->json(['status' => true, 'message' => 'Chat save success!', 'receiver_id' => $chat->receiver_id, 'chat_id' => $chat->chat_id]);
	    		}

			}else{
				return response()->json(['status' => false, 'message' => 'user not found!']);
			}
    		
    	}

    }
}
