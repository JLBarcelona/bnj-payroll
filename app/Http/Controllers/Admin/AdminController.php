<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Validator;
use App\User;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
  function index(){
  	return view('Admin.index');
  }

  function settings(){
  	return view('Admin.system_settings');
  }

  function accountsetting(){
  	return view('Admin.accountsetting');
  }

  function update_accountsetting(Request $request){
		$user_id= $request->get('user_id');
		$firstname = $request->get('firstname');
		$lastname = $request->get('lastname');
		$middlename = $request->get('middlename');
		$username = $request->get('username');
		$email_address = $request->get('email_address');

		$validator = Validator::make($request->all(), [
			'firstname' => 'required',
			'lastname' => 'required',
			'middlename' => 'required',
			'username' => 'required',
			'email_address' => 'required|email',
		]);

		if ($validator->fails()) {
			return response()->json(['status' => false, 'error' => $validator->errors()]);
		}else{
			if (!empty($user_id)) {
				$employee = User::find($user_id);
				$employee->firstname = $firstname;
				$employee->lastname = $lastname;
				$employee->middlename = $middlename;
				$employee->username = $username;
				$employee->email_address = $email_address;
				if($employee->save()){
					return response()->json(['status' => true, 'message' => 'Account Settings updated successfully!']);
				}
			}
		}
  }

  function changepassword(Request $request){

	$user_id= $request->get('user_id');
	$currentpassword = $request->get('currentpassword');
	$newpassword = $request->get('newpassword');
	$confirmpassword = $request->get('password_confirmation');

	$validator = Validator::make($request->all(), [
		'currentpassword' => 'required',
		'newpassword' => 'required',
		'password_confirmation' => 'required'
	]);

	if ($validator->fails()) {
		return response()->json(['status' => false, 'error' => $validator->errors()]);
	}
	elseif (!(Hash::check($currentpassword, Auth::user()->password))){
		$validator->errors()->add('currentpassword', 'Your current password does not match with your provided');
	    return response()->json(['status' => false, 'error' => $validator->errors()]);
	}
	elseif (strcmp($currentpassword,$newpassword)==0){
		$validator->errors()->add('newpassword', 'Your current password cannot be same with the new password');
		return response()->json(['status' => false, 'error' => $validator->errors()]);
	}
	elseif ($newpassword != $confirmpassword){
		$validator->errors()->add('password_confirmation', 'Confirm Password does not match with your new password');
		return response()->json(['status' => false, 'error' => $validator->errors()]);
	}
	else{
		if (!empty($user_id)) {
			$employee = User::find($user_id);
			$employee->password = $newpassword;
			if($employee->save()){
				return response()->json(['status' => true, 'message' => 'Password changed successfully']);
			}
		}
	}
  }
}
