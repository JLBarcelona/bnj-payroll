<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Validator;
use App\Salary;
use App\Payroll;

// PDF
use PDF;

class ReportController extends Controller
{
	function print_payslip($pay_id){
		$data = Payroll::find($pay_id);
		$date_1 = $data->report_from;
		$date_2 = $data->report_to;

		$logo = asset('img/logo.png');

		$view = \View::make('Pdf.payslip', compact('data', 'logo'));
		$html_content = $view->render();

		PDF::SetTitle('Salary for '. $date_1.' ~ '.$date_2);
		PDF::AddPage();
		PDF::writeHTML($html_content, true, false, true, false, ''); 
		PDF::Output(uniqid().'payroll-'.$date_1.'-'.$date_2.'.pdf');
	}
}
