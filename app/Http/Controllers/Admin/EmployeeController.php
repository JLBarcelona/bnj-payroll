<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Auth;
use Validator;

use App\User;
use App\Benefit;


class EmployeeController extends Controller
{
    function index(){
    	return view('Admin.user');
    }

    function payroll(){
    	return view('Admin.payroll');
    }

    function list_employee(){
		$employee = User::where('user_type', 2)->whereNull('deleted_at')->get();
		return response()->json(['status' => true, 'data' => $employee]);
	}

	function add_employee(Request $request){
		$user_id= $request->get('user_id');
	    $attendance_id = $request->get('attendance_id');
		$firstname = $request->get('firstname');
		$lastname = $request->get('lastname');
		$middlename = $request->get('middlename');
		$username = $request->get('username');
		$email_address = $request->get('email_address');
		$basic_salary = $request->get('basic_salary');

		//benefits
		$benefit_id= $request->get('benefit_id');
		$sss_number = $request->get('sss_number');
		$sss_share = $request->get('sss_share');
		$phil_health_number = $request->get('phil_health_number');
		$phil_health_share = $request->get('phil_health_share');
		$pag_ibig_number = $request->get('pag_ibig_number');
		$pag_ibig_share = $request->get('pag_ibig_share');


		$validator = Validator::make($request->all(), [
			'firstname' => 'required',
			'lastname' => 'required',
			'middlename' => 'required',
			'username' => 'required',
			'email_address' => 'required|email',
			'sss_number' => 'required',
			'sss_share' => 'required',
			'phil_health_number' => 'required',
			'phil_health_share' => 'required',
			'pag_ibig_number' => 'required',
			'pag_ibig_share' => 'required',
		]);

		if ($validator->fails()) {
			return response()->json(['status' => false, 'error' => $validator->errors()]);
		}else{
			if (!empty($user_id)) {
				$employee = User::find($user_id);
				$employee->firstname = $firstname;
				$employee->lastname = $lastname;
				$employee->middlename = $middlename;
				$employee->username = $username;
				$employee->email_address = $email_address;
				if($employee->save()){

				$benefits = new Benefit;
				$benefits->owner_id =  $employee->user_id;
				$benefits->owner_type =  get_class($employee);
				$benefits->sss_number = $sss_number;
				$benefits->sss_share = $sss_share;
				$benefits->phil_health_number = $phil_health_number;
				$benefits->phil_health_share = $phil_health_share;
				$benefits->pag_ibig_number = $pag_ibig_number;
				$benefits->pag_ibig_share = $pag_ibig_share;
				$benefits->save();

					return response()->json(['status' => true, 'message' => 'Employee updated successfully!']);
				}
			}else{
				$employee = new User;
        		$employee->attendance_id = $attendance_id;
				$employee->firstname = $firstname;
				$employee->lastname = $lastname;
				$employee->middlename = $middlename;
				$employee->username = $username;
				$employee->email_address = $email_address;
        		$employee->basic_salary = $basic_salary;
				if($employee->save()){

				$benefits = new Benefit;
				$benefits->owner_id =  $employee->user_id;
				$benefits->owner_type =  get_class($employee);
				$benefits->sss_number = $sss_number;
				$benefits->sss_share = $sss_share;
				$benefits->phil_health_number = $phil_health_number;
				$benefits->phil_health_share = $phil_health_share;
				$benefits->pag_ibig_number = $pag_ibig_number;
				$benefits->pag_ibig_share = $pag_ibig_share;
				$benefits->save();

					return response()->json(['status' => true, 'message' => 'Employee saved successfully!']);
				}
			}
		}
	}


	function delete_employee($user_id){
		$employee = User::find($user_id);
		if($employee->delete()){
			return response()->json(['status' => true, 'message' => 'Employee deleted successfully!']);
		}
	}

}
