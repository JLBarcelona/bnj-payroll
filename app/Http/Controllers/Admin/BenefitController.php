<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Validator;
use App\Benefit;
use App\User;




class BenefitController extends Controller
{
      function list_benefits(){
      $benefits = Benefit::whereNull('date_deleted')->get();
      return response()->json(['status' => true, 'data' => $benefits]);
      }

      function add_benefits(Request $request){
      $benefit_id= $request->get('benefit_id');
      $sss_number = $request->get('sss_number');
      $sss_share = $request->get('sss_share');
      $phil_health_number = $request->get('phil_health_number');
      $phil_health_share = $request->get('phil_health_share');
      $pag_ibig_number = $request->get('pag_ibig_number');
      $pag_ibig_share = $request->get('pag_ibig_share');

      $validator = Validator::make($request->all(), [
        'sss_number' => 'required',
        'sss_share' => 'required',
        'phil_health_number' => 'required',
        'phil_health_share' => 'required',
        'pag_ibig_number' => 'required',
        'pag_ibig_share' => 'required',
      ]);

      if ($validator->fails()) {
        return response()->json(['status' => false, 'error' => $validator->errors()]);
      }else{
        if (!empty($benefit_id)) {
          $benefits = Benefit::find($benefit_id);
          $benefits->sss_number = $sss_number;
          $benefits->sss_share = $sss_share;
          $benefits->phil_health_number = $phil_health_number;
          $benefits->phil_health_share = $phil_health_share;
          $benefits->pag_ibig_number = $pag_ibig_number;
          $benefits->pag_ibig_share = $pag_ibig_share;
          if($benefits->save()){
            return response()->json(['status' => true, 'message' => 'Benefits updated successfully!']);
          }
        }else{
          $user_id = User::latest()->first()->user_id;
          $benefits = new Benefit;
          $benefits->user_id = $user_id;
          $benefits->sss_number = $sss_number;
          $benefits->sss_share = $sss_share;
          $benefits->phil_health_number = $phil_health_number;
          $benefits->phil_health_share = $phil_health_share;
          $benefits->pag_ibig_number = $pag_ibig_number;
          $benefits->pag_ibig_share = $pag_ibig_share;
          if($benefits->save()){
            return response()->json(['status' => true, 'message' => 'Benefits saved successfully!']);
          }
        }
      }
      }


      function delete_benefits($benefit_id){
      $benefits = Benefit::find($benefit_id);
      if($benefits->delete()){
        return response()->json(['status' => true, 'message' => 'Benefits deleted successfully!']);
      }
      }

}
