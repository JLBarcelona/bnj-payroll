<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Validator;
use App\Holiday;


use Illuminate\Support\Facades\Hash;

class HolidayController extends Controller
{   
    function index(){
        return view('Admin.holiday');
    }

    function list_holiday(){
        $holiday = Holiday::whereNull('deleted_at')->get();
        return response()->json(['status' => true, 'data' => $holiday]);
    }
    
    function add_holiday(Request $request){
        $user = Auth::user();
        $owner_id = $user->user_id;
        $owner_type = get_class($user);
        $holiday_id= $request->get('holiday_id');
        $date = $request->get('date');
        $description = $request->get('description');
    
        $validator = Validator::make($request->all(), [
            'date' => 'required',
            'description' => 'required',
        ]);
    
        if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{
            if (!empty($holiday_id)) {
                $holiday = Holiday::find($holiday_id);
                $holiday->date = $date;
                $holiday->description = $description;
                $holiday->owner_id = $owner_id;
                $holiday->owner_type = $owner_type;
                if($holiday->save()){
                    return response()->json(['status' => true, 'message' => 'Holiday updated successfully!']);
                }
            }else{
                $holiday = new Holiday;
                $holiday->date = $date;
                $holiday->description = $description;
                $holiday->owner_id = $owner_id;
                $holiday->owner_type = $owner_type;
                if($holiday->save()){
                    return response()->json(['status' => true, 'message' => 'Holiday saved successfully!']);
                }
            }
        }
    }
    
    
    function delete_holiday($holiday_id){
        $holiday = Holiday::find($holiday_id);
        if($holiday->delete()){
            return response()->json(['status' => true, 'message' => 'Holiday deleted successfully!']);
        }
    }
}
