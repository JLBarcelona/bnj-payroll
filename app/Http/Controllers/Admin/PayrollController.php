<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Validator;

use App\User;
use App\Benefit;
use App\Payroll;
use App\DailyAttendance;

use App\Salary;

// Excel
use App\Imports\AttendanceImport;
use Maatwebsite\Excel\Facades\Excel;

use Str;
use Carbon\Carbon;



class PayrollController extends Controller
{

	function list_attendance($date = ''){
		// if (!empty($date)) {
			
		// }else{
		// 	$payroll = Payroll::orderBy('payroll_id', 'desc');
		// }

		$date_1 = Carbon::parse($date.'-1')->format('Y-m-d');
		$date_2 = Carbon::parse($date_1)->endOfMonth()->format('Y-m-d');

		$payroll = Payroll::where('report_from', $date_1)->Where('report_to', $date_2);

		if ($payroll->count() > 0) {
			$pay = $payroll->first();
			$detail = $pay->attendance;

			$print_check = Salary::where('payroll_id', $pay->payroll_id);

			if ($print_check->count() > 0) {
				return response()->json(['status' => true, 'data' => $detail, 'payroll_id' => $pay->payroll_id, 'print_ready' => true]);
			}else{
				return response()->json(['status' => true, 'data' => $detail, 'payroll_id' => $pay->payroll_id , 'print_ready' => false]);
			}
			
		}else{
			return response()->json(['status' => true, 'data' => [],  'payroll_id' => false ]);
		}

	}

	function delete_attendance($daily_attendance_id){
		$attendance = DailyAttendance::find($daily_attendance_id);
		if($attendance->delete()){
			return response()->json(['status' => true, 'message' => 'Attendance deleted successfully!']);
		}
	}


	function list_salary(){
		$salary = Salary::whereNull('date_deleted')->get();
		return response()->json(['status' => true, 'data' => $salary]);
	}

	function add_salary(Request $request){
		$status = false;
		$user = Auth::user();
		$s_config = $user->salaryconfig;

		$holiday_rate = $s_config->holiday_rate;
		$total_hour_per_day = $s_config->total_hour_per_day;

		$payroll_id = $request->get('pay_id');
		$date = $request->get('date_report');
		$validator = Validator::make($request->all(), []);

		if (empty($date)) {
			$validator->errors()->add('date_report', 'Report Date is required!');
			return response()->json(['status' => false, 'error' => $validator->errors()]);
		}else{

			$date_1 = Carbon::parse($date.'-1')->format('Y-m-d');
			$date_2 = Carbon::parse($date_1)->endOfMonth()->format('Y-m-d');
			$payroll = Payroll::where('report_from', $date_1)->Where('report_to', $date_2);

			if ($payroll->count() > 0) {
				$pay = $payroll->first();
				$detail = $pay->detail;

				foreach ($detail as $value) {
               		$check_user = User::where('attendance_id', $value->attendance_id)->whereNull('deleted_at');
					if ($check_user->count() > 0) {
							$att = $pay->attendance->where('attendance_id', $value->attendance_id);
							$user = $check_user->first();
							$owner_id = $user->user_id;
							$owner_type = get_class($user);

							// Data
							// Formula
							$days_holiday = $att->where('is_holiday', 1)->whereNull('f_time_in')->whereNull('l_time_out')->count();
							$base_salary = $user->basic_salary;
							$days_working = ($value->attend_days - $days_holiday);
							
							$holiday_work = $att->where('is_holiday', 1)->whereNotNull('f_time_in')->whereNotNull('l_time_out')->count();
							$total_ab_ot = -$att->sum('deduction_basis');
							$daily = ($base_salary/$days_working);
							$per_hour = ($daily/$total_hour_per_day);
							$holiday = ($per_hour*$holiday_rate*$holiday_work);
							$ot_ab_long = ($total_ab_ot*$per_hour);
							$ot_ab = round($ot_ab_long, 0);
							// Salary
							$salary = ($base_salary + $ot_ab + $holiday);


							$salary_user_check = Salary::where('payroll_id', $pay->payroll_id)->where('owner_id', $owner_id);

							if ($salary_user_check->count() > 0) {
								$salary_detail = $salary_user_check->first();
								$salary_detail->total_late = $total_ab_ot;
								$salary_detail->per_hour = $per_hour;
								$salary_detail->per_day = $daily;
								$salary_detail->holiday_work = $holiday_work;
								$salary_detail->holiday_salary = $holiday;
								$salary_detail->ot_ab = $ot_ab;
								$salary_detail->salary = $salary;
								$salary_detail->days_working =$days_working;
								$salary_detail->owner_id = $owner_id;
								$salary_detail->owner_type = $owner_type;
								$salary_detail->payroll_id = $pay->payroll_id;
								$salary_detail->payroll_type = get_class($pay);

								if ($salary_detail->save()) {
									$status = true;
								}
							}else{
								$salary_detail = new Salary;
								$salary_detail->total_late = $total_ab_ot;
								$salary_detail->per_hour = $per_hour;
								$salary_detail->per_day = $daily;
								$salary_detail->holiday_work = $holiday_work;
								$salary_detail->holiday_salary = $holiday;
								$salary_detail->ot_ab = $ot_ab;
								$salary_detail->salary = $salary;
								$salary_detail->days_working =$days_working;
								$salary_detail->owner_id = $owner_id;
								$salary_detail->owner_type = $owner_type;
								$salary_detail->payroll_id = $pay->payroll_id;
								$salary_detail->payroll_type = get_class($pay);

								if ($salary_detail->save()) {
									$status = true;
								}
							}



							
					}
				}
			}
		}

		if ($status) {
			return response()->json(['status' => true, 'message' => 'Salary generated successfully!']);
		}
	}


	function add_attendance(Request $request){
	$daily_attendance_id = $request->get('daily_attendance_id');
	$attendance_id= $request->get('attendance_id');
	$f_time_in = $request->get('f_time_in');
	$f_time_out = $request->get('f_time_out');
	$l_time_in = $request->get('l_time_in');
	$l_time_out = $request->get('l_time_out');
	$late_mins = $request->get('late_mins');
	$leave_early_mins = $request->get('leave_early_mins');
	$absence_mins = $request->get('absence_mins');
	$total = $request->get('total');
	$note = $request->get('note');
	$deduction_basis = $request->get('deduction_basis');

	$validator = Validator::make($request->all(), [
		'f_time_in' => 'required',
		'l_time_out' => 'required',
		'late_mins' => 'required',
		'leave_early_mins' => 'required',
		'absence_mins' => 'required',
		'total' => 'required',
		'deduction_basis' => 'required',
	]);

	if ($validator->fails()) {
		return response()->json(['status' => false, 'error' => $validator->errors()]);
	}else{
		if (!empty($daily_attendance_id)) {
			$attendance = DailyAttendance::find($daily_attendance_id);
			$attendance->f_time_in = $f_time_in;
			$attendance->f_time_out = $f_time_out;
			$attendance->l_time_in = $l_time_in;
			$attendance->l_time_out = $l_time_out;
			$attendance->late_mins = $late_mins;
			$attendance->leave_early_mins = $leave_early_mins;
			$attendance->absence_mins = $absence_mins;
			$attendance->total = $total;
			$attendance->note = $note;
			$attendance->deduction_basis = $deduction_basis;
			if($attendance->save()){
				return response()->json(['status' => true, 'message' => 'Attendance updated successfully!']);
			}
		}else{
			$attendance = new DailyAttendance;
			$attendance->f_time_in = $f_time_in;
			$attendance->f_time_out = $f_time_out;
			$attendance->l_time_in = $l_time_in;
			$attendance->l_time_out = $l_time_out;
			$attendance->late_mins = $late_mins;
			$attendance->leave_early_mins = $leave_early_mins;
			$attendance->absence_mins = $absence_mins;
			$attendance->total = $total;
			$attendance->note = $note;
			$attendance->deduction_basis = $deduction_basis;
			if($attendance->save()){
				return response()->json(['status' => true, 'message' => 'Attendance saved successfully!']);
			}
		}
	}
}



	function upload_excel(Request $request){
		// $file = $request->file;
		$status = 0;

		$payroll_id= $request->get('payroll_id');
		

		$validator = Validator::make($request->all(), [
			'file' => 'required',
		]);

		if ($validator->fails()) {
			return response()->json(['status' => false, 'error' => $validator->errors()]);
		}else{
			// dd($file);
			// Excel::import(new AttendanceImport, $file);
			if ($request->get('file') !== 'undefined') {
				$file = $request->file;

				$import = new AttendanceImport();
				$array = Excel::toArray($import, $file);

				$s = '';
				$e = '';

	            foreach ($array as $key => $value) {
	                foreach ($value as $k => $v) {
	                    if ($k == 1) {
	                    	foreach ($v as $kk => $vv) {
	                    		if ($kk == 1) {
	                    			// return response()->json(['status' => json_encode($vv)]);
										$detail = explode(' ~ ', $vv);
										$start = $detail[0];
										$end = $detail[1];
										$s = $start;
										$e = $end;

										$payroll_check = Payroll::where('report_from', $start)->where('report_to', $end)->count();

										if ($payroll_check > 0) {
											$status = 1;
										}else{
											$status = 0;
										}
	                    		}     
	                    	}
	                    }
	                }
	            }
	            
	            if ($status == 0) {
	            	Excel::import($import, $file);
	            	return response()->json(['status' => true, 'message' => 'Attendace Report successfully saved!', 'is_success' => $status]);
	            }else{
	            	 return response()->json(['status' => false, 'message' => 'Attendace Report from '.$s.' - '.$e.' is already processed!', 'is_success' => $status]);
	            }

			}else{
				$validator->errors()->add('file', 'Excel file is required!');
	            return response()->json(['status' => false, 'error' => $validator->errors()]);
			}

			

		}

	}
}
