<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Validator;
use App\SalaryConfig;
use App\LateConfig;


use Illuminate\Support\Facades\Hash;

class SystemSettingController extends Controller
{

    function add_systemsetting(Request $request){
        $user = Auth::user();
        $owner_id = $user->user_id;
        $owner_type = get_class($user);
        $config_id= $request->get('config_id');
        $total_hour_per_day = $request->get('total_hour_per_day');
        $holiday_rate = $request->get('holiday_rate');

        $validator = Validator::make($request->all(), [
            'total_hour_per_day' => 'required',
            'holiday_rate' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{
            if (!empty($config_id)) {
                $systemsetting = SalaryConfig::find($config_id);
                $systemsetting->total_hour_per_day = $total_hour_per_day;
                $systemsetting->holiday_rate = $holiday_rate;
                $systemsetting->owner_id = $owner_id;
                $systemsetting->owner_type = $owner_type;
                if($systemsetting->save()){
                    return response()->json(['status' => true, 'message' => 'Systemsetting updated successfully!']);
                }
            }else{
                $systemsetting = new SalaryConfig;
                $systemsetting->total_hour_per_day = $total_hour_per_day;
                $systemsetting->holiday_rate = $holiday_rate;
                $systemsetting->owner_id = $owner_id;
                $systemsetting->owner_type = $owner_type;
                if($systemsetting->save()){
                    return response()->json(['status' => true, 'message' => 'Systemsetting saved successfully!']);
                }
            }
        }
    }

    // Late Config

    function list_lateconfig(){
      $user = Auth::user();

    	$lateconfig = $user->lateconfig;
    	return response()->json(['status' => true, 'data' => $lateconfig]);
    }

    function add_lateconfig(Request $request){
        $user = Auth::user();
        $owner_id = $user->user_id;
        $owner_type = get_class($user);
      	$late_config_id= $request->get('late_config_id');
      	$from_time = $request->get('from_time');
      	$end_time = $request->get('end_time');
      	$deducted_from_time = $request->get('deducted_from_time');

      	$validator = Validator::make($request->all(), [
      		'from_time' => 'required',
      		'end_time' => 'required',
      		'deducted_from_time' => 'required'
      	]);

      	if ($validator->fails()) {
      		return response()->json(['status' => false, 'error' => $validator->errors()]);
      	}else{
      		if (!empty($late_config_id)) {
      			$lateconfig = LateConfig::find($late_config_id);
      			$lateconfig->from_time = $from_time;
      			$lateconfig->end_time = $end_time;
      			$lateconfig->deducted_from_time = $deducted_from_time;
            $lateconfig->owner_id = $owner_id;
            $lateconfig->owner_type = $owner_type;
      			if($lateconfig->save()){
      				return response()->json(['status' => true, 'message' => 'Late Deduction Settings updated successfully!']);
      			}
      		}else{
      			$lateconfig = new LateConfig;
      			$lateconfig->from_time = $from_time;
      			$lateconfig->end_time = $end_time;
      			$lateconfig->deducted_from_time = $deducted_from_time;
            $lateconfig->owner_id = $owner_id;
            $lateconfig->owner_type = $owner_type;
      			if($lateconfig->save()){
      				return response()->json(['status' => true, 'message' => 'Late Deduction Settings saved successfully!']);
      			}
      		}
      	}
      }

      function delete_lateconfig($late_config_id){
      	$lateconfig = LateConfig::find($late_config_id);
      	if($lateconfig->delete()){
      		return response()->json(['status' => true, 'message' => 'Late Deduction Settings deleted successfully!']);
      	}
      }

}
