<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salary extends Model
{
	protected $primaryKey = 'salary_id';

	protected $table = 'bnj_salary';

	protected $fillable = [
	'total_late','per_hour', 'per_day', 'holiday_work', 'holiday_salary', 'ot_ab', 'salary', 'days_working','created_at','updated_at','deleted_at'
	];

	protected $appends = ['payroll_date'];


	protected $hidden = ['owner_id', 'owner_type', 'payroll_id', 'payroll_type'];

	public function owner()
	{
		return $this->morphTo();
	}

	public function payroll()
	{
		return $this->morphTo();
	}

	public function getPayrollDateAttribute(){
		$pay = $this->payroll->find($this->payroll_id);

		// return $pay->report_from.' ~ '.$pay->report_to;

		return \Carbon\Carbon::parse($pay->report_from)->format('F Y');
	}

}
