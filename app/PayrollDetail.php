<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayrollDetail extends Model
{
  protected $primaryKey = 'payroll_detail_id';

  protected $table = 'bnj_payroll_detail';

  protected $fillable = [
      'attendance_id','late_times','late_mins','absent_day','holiday_work','leave_early_times','leave_early_mins', 'attend_days','created_at','updated_at','deleted_at'
      ];

  protected $hidden = ['owner_id', 'owner_type'];


   public function owner()
  {
      return $this->morphTo();
  }

}
