<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatDetail extends Model
{
  protected $primaryKey = 'chat_detail_id';

  protected $table = 'bnj_chat_detail';

  protected $fillable = [
      'message','img','created_at','updated_at','deleted_at'
      ];

  protected $hidden = ['owner_id','owner_type','chat_id','chat_type'];



  public function owner()
	{
	    return $this->morphTo();
	}

  public function chat(){
        return $this->morphTo();
  }

  public function seen()
  {
      return $this->morphMany('App\ChatSeen', 'seen')->whereNull('deleted_at');
  }

}
