<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\LateConfig;
use App\Holiday;


class DailyAttendance extends Model
{
  protected $primaryKey = 'daily_attendance_id';

  protected $table = 'bnj_daily_attendance';

  protected $fillable = [
      'attendance_id','date_attendance','f_time_in','f_time_out','l_time_in','l_time_out','late_mins','leave_early_mins','absence_mins','total','note', 'deduction_basis', 'is_holiday','created_at','updated_at','deleted_at'
      ];

  protected $appends = ['users', 'time_in_color', 'time_out_color', 'late_color', 'day_week'];

  protected $hidden = ['owner_id', 'owner_type'];

   public function owner()
  {
      return $this->morphTo();
  }


	public function getUsersAttribute(){
		return $this->user()->first();
	}

  public function getDayWeekAttribute(){
      $day = \Carbon\Carbon::create($this->date_attendance);
      $holiday = Holiday::where('date', $this->date_attendance);
      $hol_count = $holiday->count();

      if ($hol_count > 0) {
        $hol = $holiday->first();
        if (empty($this->f_time_in) || empty($this->l_time_out)) {
          return $this->date_attendance.'<span class="text-primary pointer" data-toggle="tooltip" title="'.$hol->description.'"><small><b>(Holiday)</b></small></span>';
        }else{
          return $this->date_attendance.'<span class="text-primary pointer" data-toggle="tooltip" title="'.$hol->description.'"><small><b>(Holiday Work)</b></small></span>';
        }
      }else{
        if ($day->isWeekend()) {
          return '<span class="text-primary">'.$this->date_attendance.'<small>(Weekend)</small></span>';
        }else{
          return $this->date_attendance;
        }
      }   
  }


  public function getTimeInColorAttribute(){
    if ($this->late_mins > 0) {
      return '<span class="text-warning">'.$this->f_time_in.'</span>';
    }else{
      if (!empty($this->f_time_in)) {
        return $this->f_time_in;  
      }else{
       return '<span class="text-danger">No Time In</span>';
      }
    }
  }

  public function getTimeOutColorAttribute(){
    if (!empty($this->l_time_out)) {
      return $this->l_time_out;
    }else{
     return '<span class="text-danger">No Time Out</span>';
    }
  }



  public function getLateColorAttribute(){
     if ($this->deduction_basis == 0) {
      return $this->deduction_basis;
    }else{
     return '<span class="text-danger">( '.$this->deduction_basis.' )</span>';
    }
  }

	public function user(){
		return $this->belongsTo('App\User', 'attendance_id', 'attendance_id');
	}

}
