<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Chat extends Model
{
  protected $primaryKey = 'chat_id';

  protected $table = 'bnj_chat';

  protected $fillable = [
    	'group_name','on_type','on_active','created_at','updated_at','deleted_at'
      ];

      protected $hidden = ['owner_id','owner_type'];


      public $appends = ['member_count', 'last_message', 'is_seen', 'group'];


      public function owner()
  	{
  	    return $this->morphTo();
  	}


    public function getMemberCountAttribute(){
      return $this->member()->count();
    }

	   public function member()
    {
        return $this->morphMany('App\ChatMember', 'chat')->whereNull('deleted_at');
    }

    public function detail()
    {
        return $this->morphMany('App\ChatDetail', 'chat')->whereNull('deleted_at');
    }

    public function getGroupAttribute(){
      $data = $this->member()->first();
       return $data->group_name;
    }

    public function getLastMessageAttribute(){
     $datas = $this->detail()->select('message')->latest()->get();
      foreach ($datas as $val) {
        return $val->message;
      }
    }

    public function getIsSeenAttribute(){
     $datas = $this->detail()->latest()->get();
     $user_id = Auth::user()->user_id;

      foreach ($datas as $val) {
        if ($val->seen->where('owner_id', $user_id)->count() > 0) {
          return 0;
        }else{
          return 1;
        }
      }
    }




}
