<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalaryConfig extends Model
{
  protected $primaryKey = 'config_id';

  protected $table = 'bnj_salary_config';

  protected $fillable = [
      'holiday_rate','total_hour_per_day','created_at','updated_at','deleted_at'
      ];

      protected $hidden = ['owner_id', 'owner_type'];

       public function owner()
    {
        return $this->morphTo();
    }
      
}
