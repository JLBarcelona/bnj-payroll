<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class ChatMember extends Model
{
    protected $primaryKey = 'chat_member_id';

    protected $table = 'bnj_chat_member';
  
    protected $fillable = [
        'on_type','on_active', 'is_group','created_at','updated_at','deleted_at'
        ];

    protected $hidden = ['owner_id','owner_type','chat_id','chat_type'];

    public $appends = ['group_name'];

    public function owner()
	{
	    return $this->morphTo();
	}

    public function chat(){
        return $this->morphTo();
    }


    public function getGroupNameAttribute(){
        $data = $this->chat()->first();
        $detail = $data->member;
        $name = '';

        $user_id = Auth::user()->user_id;

        $count = $data->member_count;
        if ($count > 0) { 
            if ($count == 2) {
                if (!empty($data->group_name)) {
                   return $data->group_name;
                }else{
                     foreach ($detail as $datas) {
                        if ($user_id == $datas->owner->user_id) {
                            # code...
                        }else{
                            $name .= $datas->owner->fullname .' ';
                        }
                    }
                     return $name;
                }
            }else{
               if (!empty($data->group_name)) {
                   return $data->group_name;
                }else{
                     foreach ($detail as $datas) {
                          if ($user_id == $datas->owner->user_id) {
                            # code...
                        }else{
                            $name .= $datas->owner->firstname .', ';
                        }
                    }
                     return $name;
                }
            }
        }else{
            foreach ($detail as $datas) {
                $name .= $datas->owner->fullname .' ';
            }
            return $name;
        }
    }

}
