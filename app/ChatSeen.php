<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatSeen extends Model
{
  protected $primaryKey = 'chat_seen_id';

  protected $table = 'bnj_chat_seen';

  protected $fillable = [
     	'created_at','updated_at','deleted_at'
      ];

  protected $hidden = ['owner_id','owner_type','seen_id','seen_type'];


   public function chat(){
        return $this->morphTo();
    }

    public function seen(){
        return $this->morphTo();
    }

}
